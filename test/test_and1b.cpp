#include <systemc.h>
#include <string>
#include "and1b.hpp"

using namespace std;

SC_MODULE(TestBench){
	sc_signal<sc_logic> injecter1;
	sc_signal<sc_logic> injecter2;
	sc_signal<sc_logic> observer;

	AND1b portaand;
	int check_result;
	SC_CTOR(TestBench) : portaand("portaand")
	{
		SC_THREAD(stimulus_thread);
		SC_METHOD(check);
			sensitive << observer;
			dont_initialize();
		portaand.and_in1(injecter1);
		portaand.and_in2(injecter2);
		portaand.and_out(observer);
		init_values();
	}
	void check(){
		sc_logic a =observer.read();
		std::cout << "Output from stimulus_thread: " << a << std::endl;
		if ( a != (values1[numTestRes]&values2[numTestRes]))
			check_result = 1;
		numTestRes++;
	}

	private:
	
		void stimulus_thread(){
			for (unsigned i=0; i<TEST_SIZE;i++){
				std::cout << "Stimulus_thread #"<<  i <<std::endl;
				std::cout << "Input 1 : "<< values1[i] << std::endl;
				std::cout << "Input 2 : "<< values2[i] << std::endl;
				injecter1.write(values1[i]);
				injecter2.write(values2[i]);
				wait(MS,SC_MS);
			}
		}

	unsigned numTest;
	unsigned numTestRes;
	static const unsigned MS = 10;
	static const unsigned TEST_SIZE=3;
	sc_logic values1[TEST_SIZE],values2[TEST_SIZE];

	
	void init_values(){
		numTestRes=0;
		check_result=0;
		values1[0] =1;
		values2[0] =0;
		values1[1] =1;
		values2[1] =1;
		values1[2] =1;
		values2[2] =0;
	}
};

int sc_main(int argc, char** argv){

	TestBench test("test");
	
	sc_start();
	return test.check_result;	
	
}
