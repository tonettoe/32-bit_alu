#include <systemc.h>
#include <string>
#include "or32b.hpp"

using namespace std;

SC_MODULE(TestBench){

	sc_signal<sc_lv<NBIT> > inj1;
	sc_signal<sc_lv<NBIT> > inj2;
	sc_signal<sc_lv<NBIT> > obs1;

	OR32b portor32b;
	int check_result;

	SC_CTOR(TestBench) : portor32b("portor32b"){

		SC_THREAD(stimulus_thread);
		SC_METHOD(check_res);
			sensitive << obs1;
			dont_initialize();
		portor32b.or32b_A_in(inj1);
		portor32b.or32b_B_in(inj2);
		portor32b.or32b_R_out(obs1);
		init_values();
	}

	void check_res(){
		sc_lv<NBIT> a =obs1.read();
		std::cout << "Result from check_thread: " << a << std::endl;
		std::cout << "NumTestRes " << numTestRes << endl;
		std::cout << "Now at " << sc_time_stamp() << endl;
		std::cout << std::endl;
		if ( a != resVec[numTestRes])
			check_result = 1;
		numTestRes++;

	}

	private:

		void stimulus_thread(){

			std::cout << "__STIMULUS THREAD__" << std::endl;
			for (numTest=0; numTest<TEST_SIZE;numTest++){
	
				sc_lv<NBIT> a = values1[numTest];
				sc_lv<NBIT> b = values2[numTest];

				std::cout << "Stimulus_thread #"<<  numTest <<std::endl;
				std::cout << "Input 1 : "<< a << std::endl;
				std::cout << "Input 2 : "<< b << std::endl;


				inj1.write(a);
				inj2.write(b);

				wait(MS,SC_MS);
				}
			}

	unsigned numTest;
	unsigned numTestRes;
	static const unsigned MS = 10;
	static const unsigned TEST_SIZE = 3;

	sc_lv<NBIT> values1[TEST_SIZE],values2[TEST_SIZE];

	sc_lv<NBIT> resVec[TEST_SIZE];

	void init_values(){

		std::cout << "Inizializzo Valori"<<std::endl;
		std::cout << std::endl;

		numTestRes = 0;
		check_result = 0;

		values1[0] = 15;
		values2[0] = 0;
		resVec[0] = values1[0] | values2[0];

		values1[1] = 128;
		values2[1] = 3;
		resVec[1] = values1[1] | values2[1];


		values1[2] = 17;
		values2[2] = 0;
		resVec[2] =  values1[2] | values2[2];
	}


};

int sc_main(int argc, char** argv){

	TestBench test("test");
	
	sc_start();

	return test.check_result;

}
