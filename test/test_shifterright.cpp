#include <systemc.h>
#include <string>
#include "shifterright.hpp"

using namespace std;

SC_MODULE(TestBench){
	sc_signal<sc_lv<NBIT> > injecter1;
	sc_signal<sc_lv<NBIT> > observer;

	SHIFTERRIGHT slr;
	int check_result;
	SC_CTOR(TestBench) : slr("slr")
	{
		SC_THREAD(stimulus_thread);
		SC_METHOD(check);
			sensitive << observer;
			dont_initialize();
		slr.shifterright_A(injecter1);
		slr.shifterright_R(observer);
		init_values();
	}
	void check(){
		sc_lv<NBIT> a =observer.read();
		std::cout << "Output from stimulus_thread: " << a << std::endl;
		sc_lv<NBIT> temp =values1[numTestRes];
		if ( a != (temp >> 1))
			check_result = 1;
		numTestRes++;
	}

	private:
	
		void stimulus_thread(){
			for (unsigned i=0; i<TEST_SIZE;i++){
				std::cout << "Stimulus_thread #"<<  i <<std::endl;
				std::cout << "Input: "<< values1[i] << std::endl;
				injecter1.write(values1[i]);
				wait(MS,SC_MS);
			}
		}

	unsigned numTest;
	unsigned numTestRes;
	static const unsigned MS = 10;
	static const unsigned TEST_SIZE=3;
	sc_lv<NBIT> values1[TEST_SIZE];

	
	void init_values(){
		numTestRes=0;
		check_result=0;

		values1[0] =12;

		values1[1] =354861;

		values1[2] =3548;

	}
};

int sc_main(int argc, char** argv){

	TestBench test("test");
	
	sc_start();
	return test.check_result;	
	
}
