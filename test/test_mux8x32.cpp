#include <systemc.h>
#include <string>
#include "mux8x32.hpp"

using namespace std;

SC_MODULE(TestBench){

	sc_signal<sc_lv<NBIT> > inj0;
	sc_signal<sc_lv<NBIT> > inj1;
	sc_signal<sc_lv<NBIT> > inj2;
	sc_signal<sc_lv<NBIT> > inj3;
	sc_signal<sc_lv<NBIT> > inj4;
	sc_signal<sc_lv<NBIT> > inj5;
	sc_signal<sc_lv<NBIT> > inj6;
	sc_signal<sc_lv<NBIT> > inj7;

	sc_signal<sc_lv<NSEL> > injsel;

	sc_signal<sc_lv<NBIT> > obs1;

	MUX8x32 mm;;
	int check_result;

	SC_CTOR(TestBench) : mm("mm"){

		SC_THREAD(stimulus_thread);
		SC_METHOD(check_res);
			sensitive << obs1;
			dont_initialize();
		mm.mux8x32_0(inj0);
		mm.mux8x32_1(inj1);
		mm.mux8x32_2(inj2);
		mm.mux8x32_3(inj3);
		mm.mux8x32_4(inj4);
		mm.mux8x32_5(inj5);
		mm.mux8x32_6(inj6);
		mm.mux8x32_7(inj7);

		mm.mux8x32_sel(injsel);
		mm.mux8x32_OUT(obs1);
		init_values();
	}

	void check_res(){
		sc_lv<NBIT> a =obs1.read();
		std::cout << "Result from check_thread: " << a << std::endl;
		std::cout << "NumTestRes " << numTestRes << endl;
		std::cout << "Now at " << sc_time_stamp() << endl;
		std::cout << std::endl;
//		if ( a != resVec[numTestRes])
//			check_result = 1;
		numTestRes++;

	}

	private:

		void stimulus_thread(){

			std::cout << "__STIMULUS THREAD__" << std::endl;
			for (numTest=0; numTest<TEST_SIZE;numTest++){
	
				sc_lv<NBIT> in0 = values0[numTest];
				sc_lv<NBIT> in1 = values1[numTest];
				sc_lv<NBIT> in2 = values2[numTest];
				sc_lv<NBIT> in3 = values3[numTest];
				sc_lv<NBIT> in4 = values4[numTest];
				sc_lv<NBIT> in5 = values5[numTest];
				sc_lv<NBIT> in6 = values6[numTest];
				sc_lv<NBIT> in7 = values7[numTest];

				sc_lv<NSEL> insel = valuessel[numTest];

				inj0.write(in0);
				inj1.write(in1);
				inj2.write(in2);
				inj3.write(in3);
				inj4.write(in4);
				inj5.write(in5);
				inj6.write(in6);
				inj7.write(in7);

				injsel.write(insel);
								
				wait(MS,SC_MS);
				}
			}

	unsigned numTest;
	unsigned numTestRes;
	static const unsigned MS = 10;
	static const unsigned TEST_SIZE = 3;

	sc_lv<NBIT> values0[TEST_SIZE],values1[TEST_SIZE],values2[TEST_SIZE],values3[TEST_SIZE],values4[TEST_SIZE],values5[TEST_SIZE],values6[TEST_SIZE],values7[TEST_SIZE];
	sc_lv<NSEL> valuessel[TEST_SIZE];

	sc_lv<NBIT> resVec[TEST_SIZE];

	void init_values(){

		std::cout << "Inizializzo Valori"<<std::endl;
		std::cout << std::endl;

		numTestRes = 0;
		check_result = 0;

		values0[0] = 2;
		values1[0] = 7;
		values2[0] = 48;
		values3[0] = 45867;
		values4[0] = 54685;
		values5[0] = 64568489;
		values6[0] = 0;
		values7[0] = 554;
		valuessel[0] = 1;

		values0[1] = 5;
		values1[1] = 1;
		values2[1] = 15;
		values3[1] = 454867;
		values4[1] = 574685;
		values5[1] = 664568489;
		values6[1] = 30;
		values7[1] = 5154;
		valuessel[1] = 0;

		values0[2] = 155;
		values1[2] = 244;
		values2[2] = 4658;
		values3[2] = 4584567;
		values4[2] = 1;
		values5[2] = 645618489;
		values6[2] = 10;
		values7[2] = 5154;
		valuessel[2] = 4;

	}


};

int sc_main(int argc, char** argv){

	TestBench test("test");
	
	sc_start();

	return test.check_result;

}
	
