#include <systemc.h>
#include <string>
#include "not32b.hpp"

using namespace std;

SC_MODULE(TestBench){

	sc_signal<sc_lv<NBIT> > inj1;
	sc_signal<sc_lv<NBIT> > obs1;

	NOT32b portanot32b;
	int check_result;

	SC_CTOR(TestBench) : portanot32b("portanot32b"){

		SC_THREAD(stimulus_thread);
		SC_METHOD(check_res);
			sensitive << obs1;
			dont_initialize();
		portanot32b.not32b_A(inj1);
		portanot32b.not32b_R(obs1);
		init_values();
	}

	void check_res(){
		sc_lv<NBIT> a =obs1.read();
		std::cout << "Result from check_thread: " << a << std::endl;
		std::cout << "NumTestRes " << numTestRes << endl;
		std::cout << "Now at " << sc_time_stamp() << endl;
		std::cout << std::endl;
		if ( a != resVec[numTestRes])
			check_result = 1;
		numTestRes++;

	}

	private:

		void stimulus_thread(){

			std::cout << "__STIMULUS THREAD__" << std::endl;
			for (numTest=0; numTest<TEST_SIZE;numTest++){

				sc_lv<NBIT> a = values1[numTest];
				std::cout << "Stimulus_thread #"<<  numTest <<std::endl;
				std::cout << "Input 1 : "<< a << std::endl;
				inj1.write(a);
				wait(MS,SC_MS);
				}
			}

	unsigned numTest;
	unsigned numTestRes;
	static const unsigned MS = 10;
	static const unsigned TEST_SIZE = 4;

	sc_lv<NBIT> values1[TEST_SIZE];
	sc_lv<NBIT> resVec[TEST_SIZE];

	void init_values(){

		std::cout << "Inizializzo Valori"<<std::endl;
		std::cout << std::endl;

		numTestRes = 0;
		check_result = 0;

		values1[0] = 15;
		resVec[0] = ~15;

		values1[1] = 1;
		resVec[1] = ~1;


		values1[2] = 41412321;
		resVec[2] =  ~41412321;


		values1[3] = 414123;
		resVec[3] =  ~414123;
	}
};

int sc_main(int argc, char** argv){

	TestBench test("test");
	
	sc_start();

	return test.check_result;

}
