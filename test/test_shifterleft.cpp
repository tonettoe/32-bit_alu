#include <systemc.h>
#include <string>
#include "shifterleft.hpp"

using namespace std;

SC_MODULE(TestBench){
	sc_signal<sc_lv<NBIT> > injecter1;
	sc_signal<sc_lv<NBIT> > observer;

	SHIFTERLEFT sll;
	int check_result;
	SC_CTOR(TestBench) : sll("sll")
	{
		SC_THREAD(stimulus_thread);
		SC_THREAD(check);
			sensitive << observer;
			dont_initialize();
		sll.shifterleft_A(injecter1);
		sll.shifterleft_R(observer);
		init_values();
	}
	void check(){
		sc_lv<NBIT> a =observer.read();
		std::cout << "Output from stimulus_thread: " << a << std::endl;
		unsigned temp =values1[numTestRes].to_uint() << 1 ;
		if ( a != temp )
			check_result = 1;
		numTestRes++;
	}

	private:
	
		void stimulus_thread(){
			for (unsigned i=0; i<TEST_SIZE;i++){
				std::cout << "Stimulus_thread #"<<  i <<std::endl;
				std::cout << "Input : "<< values1[i] << std::endl;
				injecter1.write(values1[i]);
				wait(MS,SC_MS);
			}
		}

	unsigned numTest;
	int numTestRes;
	static const unsigned MS = 10;
	static const unsigned TEST_SIZE=3;
	sc_lv<NBIT> values1[TEST_SIZE];


	
	void init_values(){
		numTestRes=0;
		check_result=0;

		values1[0] =12;

		values1[1] =354861;

		values1[2] =95;


	}
};

int sc_main(int argc, char** argv){

	TestBench test("test");
	
	sc_start();
	return test.check_result;	
	
}
