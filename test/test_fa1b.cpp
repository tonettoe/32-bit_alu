#include <systemc.h>
#include <string>
#include "fa1b.hpp"

using namespace std;

SC_MODULE(TestBench){
	sc_signal<sc_logic> inj1;	//op1
	sc_signal<sc_logic> inj2;	//op2
	sc_signal<sc_logic> inj3;	//carry in
	sc_signal<sc_logic> obs1;	//result	
	sc_signal<sc_logic> obs2;	//carry out

	FA1b fulladder1;

	// Flags separati per la verifica del carry e del risultato
	int check_result;		
	int check_carry;
		
	SC_CTOR(TestBench) : fulladder1("fulladder1")
	{
		SC_THREAD(stimulus_thread);
		SC_METHOD(check_res);
			sensitive << obs1;
			dont_initialize();
		SC_METHOD(check_car);
			sensitive << obs2;
			dont_initialize();
		fulladder1.fa1b_a(inj1);
		fulladder1.fa1b_b(inj2);
		fulladder1.fa1b_carryin(inj3);
		fulladder1.fa1b_res(obs1);
		fulladder1.fa1b_carryout(obs2);
		init_values();
	}

	void check_res(){
		sc_logic a =obs1.read();
		std::cout << "Result from check_thread: " << a << std::endl;
		std::cout << "Now at " << sc_time_stamp() << endl;
		std::cout << std::endl;
		if ( a != resVec[numTestRes])
			check_result = 1;
		numTestRes++;
	}

	void check_car(){
		sc_logic b =obs2.read();
		std::cout << "Carry from check_thread: " << b << std::endl;
		std::cout << "Now at " << sc_time_stamp() << endl;
		std::cout << std::endl;
		if (b != carryVec[numTestCar])
			check_carry = 1;
		numTestCar++;
	}

	private:
	
		void stimulus_thread(){
	
			for (numTest=0; numTest<TEST_SIZE;numTest++){
				check_result = 0;
				
				sc_logic a = values1[numTest];
				sc_logic b = values2[numTest];
				sc_logic f = values3[numTest];
	
				std::cout << "Stimulus_thread #"<<  numTest <<std::endl;
				std::cout << "Input 1 : "<< a << std::endl;
				std::cout << "Input 2 : "<< b << std::endl;
				std::cout << "Input 3 : "<< f << std::endl;
				std::cout << "Now at " << sc_time_stamp() << endl;
				std::cout << std::endl;

				inj1.write(a);
				inj2.write(b);
				inj3.write(f);
				
				wait(MS,SC_MS);
			}
		}
	
	unsigned numTest;
	unsigned numTestRes;
	unsigned numTestCar;
	static const unsigned MS = 10;
	static const unsigned TEST_SIZE=4;
	// Vettori di ingresso
	sc_logic values1[TEST_SIZE],values2[TEST_SIZE],values3[TEST_SIZE];
	// Vettori di uscita
	sc_logic carryVec[TEST_SIZE],resVec[TEST_SIZE];

	
	void init_values(){
		
		numTestRes=0;
		numTestCar = 0;
		check_result= 0;
		check_carry = 0;
		
		values1[0] =1;
		values2[0] =0;
		values3[0] =1;
		resVec[0]= 0;
		carryVec[0] = 1;

		values1[1] =0;
		values2[1] =1;
		values3[1] =0;
		resVec[1]= 1;
		carryVec[1] = 0;

		values1[2] =1;
		values2[2] =1;
		values3[2] =0;
		resVec[2]= 0;
		carryVec[2] = 1;

		values1[3] =0;
		values2[3] =0;
		values3[3] =0;
		resVec[3]= 0;
		carryVec[3] = 0;
	}
};

int sc_main(int argc, char** argv){

	TestBench test("test");
	
	sc_start();

	return (test.check_result & test.check_carry);
}
