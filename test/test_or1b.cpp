#include <systemc.h>
#include <string>
#include "or1b.hpp"

using namespace std;

SC_MODULE(TestBench){
	sc_signal<sc_logic> injecter1;
	sc_signal<sc_logic> injecter2;
	sc_signal<sc_logic> observer;

	OR1b portaor;
	int check_result;
	SC_CTOR(TestBench) : portaor("portaor"){
		SC_THREAD(stimulus_thread);
		SC_METHOD(check);
			sensitive << observer;
			dont_initialize();
		portaor.or_in1(injecter1);
		portaor.or_in2(injecter2);
		portaor.or_out(observer);
		init_values();
	}

	void check(){

		sc_logic a =observer.read();
		std::cout << "Output from stimulus_thread: " << a << std::endl;
		if ( a != (values1[numTestRes]|values2[numTestRes]))
			check_result = 1;
		numTestRes++;
	}

	private:
		void stimulus_thread(){
			for (numTest=0; numTest<TEST_SIZE;numTest++){
				std::cout << "Stimulus_thread #"<<  numTest <<std::endl;
				std::cout << "Input 1 : "<< values1[numTest] << std::endl;
				std::cout << "Input 2 : "<< values2[numTest] << std::endl;
				injecter1.write(values1[numTest]);
				injecter2.write(values2[numTest]);
				wait(MS,SC_MS);
			}
		}

	unsigned numTest;
	unsigned numTestRes;
	static const unsigned MS = 10;
	static const unsigned TEST_SIZE=3;
	sc_logic values1[TEST_SIZE],values2[TEST_SIZE];

	void init_values(){
		numTestRes =0;
		check_result=0;
		values1[0] =1;
		values2[0] =0;
		values1[1] =0;
		values2[1] =0;
		values1[2] =0;
		values2[2] =1;
	}
};

int sc_main(int argc, char** argv){

	TestBench test("test");
	
	sc_start();

	return test.check_result;
}
