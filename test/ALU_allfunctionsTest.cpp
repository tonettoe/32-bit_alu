// Alu all function Test

#include <systemc.h>
#include <string>
#include "alu.hpp"

using namespace std;

SC_MODULE(TestBench){

	sc_signal<sc_lv<NBIT> > inj1;	//op1
	sc_signal<sc_lv<NBIT> > inj2;	//op2
	sc_signal<sc_lv<NOP> > inj3;	//carry in
	sc_signal<sc_lv<NBIT> > obs1;	//result	
	sc_signal<sc_logic> obs2;	//carry out

	ALU alucomponent;

	// Flags separati per la verifica del carry e del risultato
	int check_result;		
	int check_carry;


	SC_CTOR(TestBench)  :  alucomponent("alucomponent")
	{
		SC_THREAD(stimulus_thread);
		SC_METHOD(check_res);
			sensitive << obs1;
			dont_initialize();
		SC_METHOD(check_car);
			sensitive << obs2;
			dont_initialize();
		alucomponent.A(inj1);
		alucomponent.B(inj2);
		alucomponent.OP(inj3);

		alucomponent.RES(obs1);
		alucomponent.CARRY(obs2);
		init_values();
	}

	void check_res(){
		sc_lv<NBIT> a =obs1.read();
		std::cout << st_res[numTestRes] << std::endl;
		std::cout << "Input 1: " << values1[numTestRes] << std::endl;
		std::cout << "Input 2: " << values2[numTestRes] << std::endl;
		std::cout << "Input Op: " << values3[numTestRes] << std::endl;
		std::cout << "Result: " << a << std::endl;
		std::cout << "of stimulus thread number " << numTestRes << endl;
		std::cout << "Now at " << sc_time_stamp() << endl;
		std::cout << std::endl;
		numTestRes++;

	}

	void check_car(){
		sc_logic b =obs2.read();
		std::cout << "Carry : " << b << std::endl;
		std::cout << "of stimulus thread number " << numTestRes-1 << endl;
		std::cout << "Now at " << sc_time_stamp() << endl;
		std::cout << std::endl;
	}

	private:	
		
		void stimulus_thread(){

			std::cout << "__STIMULUS THREAD__" << std::endl;
			for (numTest=0; numTest<TEST_SIZE;numTest++){
	
				sc_lv<NBIT> a = values1[numTest];
				sc_lv<NBIT> b = values2[numTest];
				sc_lv<NOP> c = values3[numTest];

				std::cout << "Stimulus_thread #"<<  numTest <<std::endl;
				std::cout << std::endl;
				inj1.write(a);
				inj2.write(b);
				inj3.write(c);

				wait(NS,SC_NS);
				}
			}
	
	unsigned numTest;
	unsigned numTestRes;
	static const unsigned NS = 2;	
	static const unsigned TEST_SIZE = 10;

	//Vettori di ingresso
	sc_lv<NBIT> values1[TEST_SIZE],values2[TEST_SIZE];
	sc_lv<NOP> values3[TEST_SIZE];

	//Vettori di uscita
	sc_lv<NBIT> resVec[TEST_SIZE];
	sc_logic carryVec[TEST_SIZE];
	string st_res[TEST_SIZE];
	
		

	void init_values(){

		numTestRes = 0;
		check_result= 0;

		values1[0] = 15;
		values2[0] = 0;
		values3[0] = 0;
		st_res[0] = "NOP function Result ";


		values1[1] = 84953;
		values2[1] = 0;
		values3[1] = 1;
		st_res[1] = "NOT function Result ";


		values1[2] = 63;
		values2[2] = 10;
		values3[2] = 2;
		st_res[2] = "AND function Result ";

		values1[3] = 458;
		values2[3] = 430;
		values3[3] = 3;
		st_res[3] = "OR function Result ";


		values1[4] = 15;
		values2[4] = 10;
		values3[4] = 4;
		st_res[4] = "XOR function Result ";

		values1[5] = 63;
		values2[5] = 1;
		values3[5] = 5;
		st_res[5] = "SHIFTER RIGHT function Result ";

		values1[6] = 10;
		values2[6] = 2;
		values3[6] = 6;
		st_res[6] = "SHIFTER LEFT function Result ";

		values1[7] = 7;
		values2[7] = 2;
		values3[7] = 7;
		st_res[7] = "ADDER function Result ";

		values1[8] = "11111111111111111111111111111111";
		values2[8] = 2;
		values3[8] = 7;
		st_res[8] = "ADDER function Result ";

		values1[9] = 0;
		values2[9] = 0;
		values3[9] = 0;


	}
};

int sc_main(int argc, char** argv){

	TestBench test("test");
	
	sc_start();

	return 0;

}
