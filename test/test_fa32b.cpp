#include <systemc.h>
#include <string>
#include "fa32b.hpp"

using namespace std;

SC_MODULE(TestBench){

	sc_signal<sc_lv<NBIT> > inj1;	//op1
	sc_signal<sc_lv<NBIT> > inj2;	//op2
	sc_signal<sc_logic> inj3;	//carry in
	sc_signal<sc_lv<NBIT> > obs1;	//result	
	sc_signal<sc_logic> obs2;	//carry out

	FA32b fulladder321;

	// Flags separati per la verifica del carry e del risultato
	int check_result;		
	int check_carry;


	SC_CTOR(TestBench)  :  fulladder321("fulladder321")
	{
		SC_THREAD(stimulus_thread);
		SC_METHOD(check_res);
			sensitive << obs1;
			dont_initialize();
		SC_METHOD(check_car);
			sensitive << obs2;
			dont_initialize();
		fulladder321.fa32b_A_in(inj1);
		fulladder321.fa32b_B_in(inj2);
		fulladder321.fa32b_c0_in(inj3);

		fulladder321.fa32b_R_out(obs1);
		fulladder321.fa32b_c32_out(obs2);
		init_values();
	}

	void check_res(){
		sc_lv<NBIT> a =obs1.read();
		std::cout << "Result from check_thread: " << a << std::endl;
		std::cout << "NumTestRes " << numTestRes << endl;
		std::cout << "Now at " << sc_time_stamp() << endl;
		std::cout << std::endl;
		if ( a != resVec[numTestRes])
			check_result = 1;
		numTestRes++;
	}

	void check_car(){
		sc_logic b =obs2.read();
		std::cout << "Carry from check_thread: " << b << std::endl;
		std::cout << "NumTestCar " << numTestCar << endl;
		std::cout << "Now at " << sc_time_stamp() << endl;
		std::cout << std::endl;
		if (b != carryVec[numTestCar])
			check_carry = 1;
		numTestCar++;
	}

	private:	
		
		void stimulus_thread(){

			std::cout << "__STIMULUS THREAD__" << std::endl;
			for (numTest=0; numTest<TEST_SIZE;numTest++){
	
				sc_lv<NBIT> a = values1[numTest];
				sc_lv<NBIT> b = values2[numTest];
				sc_logic c = values3[numTest];

				std::cout << "Stimulus_thread #"<<  numTest <<std::endl;
				std::cout << "Input 1 : "<< a << std::endl;
				std::cout << "Input 2 : "<< b << std::endl;
				std::cout << "Input 3 : "<< c << std::endl;
				std::cout << std::endl;
				inj1.write(a);
				inj2.write(b);
				inj3.write(c);

				wait(MS,SC_MS);
				}
			}
	
	unsigned numTest;
	unsigned numTestRes;
	unsigned numTestCar;
	static const unsigned MS = 20;	
	static const unsigned TEST_SIZE = 4;

	//Vettori di ingresso
	sc_lv<NBIT> values1[TEST_SIZE],values2[TEST_SIZE];
	sc_logic values3[TEST_SIZE];

	//Vettori di uscita
	sc_lv<NBIT> resVec[TEST_SIZE];
	sc_logic carryVec[TEST_SIZE];

	void init_values(){
		std::cout << "Inizializzo Valori"<<std::endl;
		std::cout << std::endl;
		
		numTestRes = 0;
		numTestCar = 0;
		check_result= 0;
		check_carry = 0;

		values1[0] = 156;
		values2[0] = 24;
		values3[0] = 0;
		resVec[0]= 180;
		carryVec[0] = 0;

		values1[1] = 8;
		values2[1] = 13;
		values3[1] = 1;
		resVec[1]= 0;
		carryVec[1] = 1;

		values1[2] = 49;
		values2[2] = 78;
		values3[2] = 1;
		resVec[2]= 4;
		carryVec[2] = 0;

		values1[3] = 0;
		values2[3] = "11111111111111111111111111111111";
		values3[3] = 1;
		resVec[3]= 0;
		carryVec[3] = 1;


	}
};

int sc_main(int argc, char** argv){

	TestBench test("test");
	
	sc_start();

	return (test.check_result & test.check_carry);

}
