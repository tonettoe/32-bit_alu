#ifndef OR32B_HPP
#define OR32B_HPP

#include <systemc.h>
#include <string>
#include "or1b.hpp"
#include "conv32to1.hpp"
#include "conv1to32.hpp"

#define NBIT 32

SC_MODULE(OR32b) {

	sc_in<sc_lv<NBIT> > or32b_A_in, or32b_B_in;
	sc_out<sc_lv<NBIT> > or32b_R_out;

	//inner signal
	sc_signal<sc_logic> oa0,oa1,oa2,oa3,oa4,oa5,oa6,oa7,oa8,oa9,oa10,oa11,oa12,oa13,oa14,oa15,oa16,oa17,oa18,oa19,oa20,oa21,oa22,oa23,oa24,oa25,oa26,oa27,oa28,oa29,oa30,oa31;
	sc_signal<sc_logic> ob0,ob1,ob2,ob3,ob4,ob5,ob6,ob7,ob8,ob9,ob10,ob11,ob12,ob13,ob14,ob15,ob16,ob17,ob18,ob19,ob20,ob21,ob22,ob23,ob24,ob25,ob26,ob27,ob28,ob29,ob30,ob31;
	sc_signal<sc_logic> oo0,oo1,oo2,oo3,oo4,oo5,oo6,oo7,oo8,oo9,oo10,oo11,oo12,oo13,oo14,oo15,oo16,oo17,oo18,oo19,oo20,oo21,oo22,oo23,oo24,oo25,oo26,oo27,oo28,oo29,oo30,oo31;	
	sc_signal<sc_lv<NBIT> > conv1in_frominput, conv2in_frominput, or32b_R_forout;
	
	
				
	//inner components
	OR1b o0,o1,o2,o3,o4,o5,o6,o7,o8,o9,o10,o11,o12,o13,o14,o15,o16,o17,o18,o19,o20,o21,o22,o23,o24,o25,o26,o27,o28,o29,o30,o31;
	conv1to32 conv1in, conv2in;
	conv32to1 convout;
	SC_CTOR(OR32b) : o0("o0"),o1("o1"),o2("o2"),o3("o3"),o4("o4"),o5("o5"),o6("o6"),o7("o7"),o8("o8"),o9("o9"),o10("o10"),o11("o11"),o12("o12"),o13("o13"),o14("o14"),o15("o15"),o16("o16"),o17("o17"),o18("o18"),o19("o19"),o20("o20"),o21("o21"),o22("o22"),o23("o23"),o24("o24"),o25("o25"),o26("o26"),o27("o27"),o28("o28"),o29("o29"),o30("o30"),o31("o31"),conv1in("conv1in"),conv2in("conv2in"),convout("convout")
				{

				o0.or_in1(oa0);
				o0.or_in2(ob0);
				o0.or_out(oo0);
				
				o1.or_in1(oa1);
				o1.or_in2(ob1);
				o1.or_out(oo1);

				o2.or_in1(oa2);
				o2.or_in2(ob2);
				o2.or_out(oo2);
				
				o3.or_in1(oa3);
				o3.or_in2(ob3);
				o3.or_out(oo3);
				
				o4.or_in1(oa4);
				o4.or_in2(ob4);
				o4.or_out(oo4);
				
				o5.or_in1(oa5);
				o5.or_in2(ob5);
				o5.or_out(oo5);
				
				o6.or_in1(oa6);
				o6.or_in2(ob6);
				o6.or_out(oo6);
				
				o7.or_in1(oa7);
				o7.or_in2(ob7);
				o7.or_out(oo7);
				
				o8.or_in1(oa8);
				o8.or_in2(ob8);
				o8.or_out(oo8);
				
				o9.or_in1(oa9);
				o9.or_in2(ob9);
				o9.or_out(oo9);
				
				o10.or_in1(oa10);
				o10.or_in2(ob10);
				o10.or_out(oo10);
				
				o11.or_in1(oa11);
				o11.or_in2(ob11);
				o11.or_out(oo11);
				
				o12.or_in1(oa12);
				o12.or_in2(ob12);
				o12.or_out(oo12);
				
				o13.or_in1(oa13);
				o13.or_in2(ob13);
				o13.or_out(oo13);
				
				o14.or_in1(oa14);
				o14.or_in2(ob14);
				o14.or_out(oo14);
				
				o15.or_in1(oa15);
				o15.or_in2(ob15);
				o15.or_out(oo15);
				
				o16.or_in1(oa16);
				o16.or_in2(ob16);
				o16.or_out(oo16);
				
				o17.or_in1(oa17);
				o17.or_in2(ob17);
				o17.or_out(oo17);
				
				o18.or_in1(oa18);
				o18.or_in2(ob18);
				o18.or_out(oo18);
				
				o19.or_in1(oa19);
				o19.or_in2(ob19);
				o19.or_out(oo19);
				// o20
				o20.or_in1(oa20);
				o20.or_in2(ob20);
				o20.or_out(oo20);
				
				o21.or_in1(oa21);
				o21.or_in2(ob21);
				o21.or_out(oo21);
				
				o22.or_in1(oa22);
				o22.or_in2(ob22);
				o22.or_out(oo22);
				
				o23.or_in1(oa23);
				o23.or_in2(ob23);
				o23.or_out(oo23);
				
				o24.or_in1(oa24);
				o24.or_in2(ob24);
				o24.or_out(oo24);
				
				o25.or_in1(oa25);
				o25.or_in2(ob25);
				o25.or_out(oo25);
				
				o26.or_in1(oa26);
				o26.or_in2(ob26);
				o26.or_out(oo26);
				
				o27.or_in1(oa27);
				o27.or_in2(ob27);
				o27.or_out(oo27);
				
				o28.or_in1(oa28);
				o28.or_in2(ob28);
				o28.or_out(oo28);
				
				o29.or_in1(oa29);
				o29.or_in2(ob29);
				o29.or_out(oo29);
				
				o30.or_in1(oa30);
				o30.or_in2(ob30);
				o30.or_out(oo30);
				
				o31.or_in1(oa31);
				o31.or_in2(ob31);
				o31.or_out(oo31);

				conv1in.conv_in(conv1in_frominput);	
				conv1in.conv0_out(oa0);
				conv1in.conv1_out(oa1);
				conv1in.conv2_out(oa2);
				conv1in.conv3_out(oa3);
				conv1in.conv4_out(oa4);
				conv1in.conv5_out(oa5);
				conv1in.conv6_out(oa6);
				conv1in.conv7_out(oa7);
				conv1in.conv8_out(oa8);
				conv1in.conv9_out(oa9);
				conv1in.conv10_out(oa10);
				conv1in.conv11_out(oa11);
				conv1in.conv12_out(oa12);
				conv1in.conv13_out(oa13);
				conv1in.conv14_out(oa14);
				conv1in.conv15_out(oa15);
				conv1in.conv16_out(oa16);
				conv1in.conv17_out(oa17);
				conv1in.conv18_out(oa18);
				conv1in.conv19_out(oa19);
				conv1in.conv20_out(oa20);
				conv1in.conv21_out(oa21);
				conv1in.conv22_out(oa22);
				conv1in.conv23_out(oa23);
				conv1in.conv24_out(oa24);
				conv1in.conv25_out(oa25);
				conv1in.conv26_out(oa26);
				conv1in.conv27_out(oa27);
				conv1in.conv28_out(oa28);
				conv1in.conv29_out(oa29);
				conv1in.conv30_out(oa30);
				conv1in.conv31_out(oa31);

				conv2in.conv_in(conv2in_frominput);
				conv2in.conv0_out(ob0);
				conv2in.conv1_out(ob1);
				conv2in.conv2_out(ob2);
				conv2in.conv3_out(ob3);
				conv2in.conv4_out(ob4);
				conv2in.conv5_out(ob5);
				conv2in.conv6_out(ob6);
				conv2in.conv7_out(ob7);
				conv2in.conv8_out(ob8);
				conv2in.conv9_out(ob9);
				conv2in.conv10_out(ob10);
				conv2in.conv11_out(ob11);
				conv2in.conv12_out(ob12);
				conv2in.conv13_out(ob13);
				conv2in.conv14_out(ob14);
				conv2in.conv15_out(ob15);
				conv2in.conv16_out(ob16);
				conv2in.conv17_out(ob17);
				conv2in.conv18_out(ob18);
				conv2in.conv19_out(ob19);
				conv2in.conv20_out(ob20);
				conv2in.conv21_out(ob21);
				conv2in.conv22_out(ob22);
				conv2in.conv23_out(ob23);
				conv2in.conv24_out(ob24);
				conv2in.conv25_out(ob25);
				conv2in.conv26_out(ob26);
				conv2in.conv27_out(ob27);
				conv2in.conv28_out(ob28);
				conv2in.conv29_out(ob29);
				conv2in.conv30_out(ob30);
				conv2in.conv31_out(ob31);

				convout.conv_out(or32b_R_forout);
				convout.conv0_in(oo0);
				convout.conv1_in(oo1);
				convout.conv2_in(oo2);
				convout.conv3_in(oo3);
				convout.conv4_in(oo4);
				convout.conv5_in(oo5);
				convout.conv6_in(oo6);
				convout.conv7_in(oo7);
				convout.conv8_in(oo8);
				convout.conv9_in(oo9);
				convout.conv10_in(oo10);
				convout.conv11_in(oo11);
				convout.conv12_in(oo12);
				convout.conv13_in(oo13);
				convout.conv14_in(oo14);
				convout.conv15_in(oo15);
				convout.conv16_in(oo16);
				convout.conv17_in(oo17);
				convout.conv18_in(oo18);
				convout.conv19_in(oo19);
				convout.conv20_in(oo20);
				convout.conv21_in(oo21);
				convout.conv22_in(oo22);
				convout.conv23_in(oo23);
				convout.conv24_in(oo24);
				convout.conv25_in(oo25);
				convout.conv26_in(oo26);
				convout.conv27_in(oo27);
				convout.conv28_in(oo28);
				convout.conv29_in(oo29);
				convout.conv30_in(oo30);
				convout.conv31_in(oo31);

				SC_THREAD(or32b_thread);
					sensitive << or32b_A_in << or32b_B_in;
				;

			}
		void or32b_thread(void);
};
#endif

