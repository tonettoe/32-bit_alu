#ifndef SHIFTERLEFT_HPP
#define SHIFTERLEFT_HPP

#include <systemc.h>
#include <string>


#define NBIT 32

SC_MODULE(SHIFTERLEFT) {

	sc_in<sc_lv<NBIT> > shifterleft_A;
	sc_out<sc_lv<NBIT> > shifterleft_R;

	
	SC_CTOR(SHIFTERLEFT){
		SC_THREAD(shifterleft_thread);
			sensitive << shifterleft_A;
		}	
		void shifterleft_thread(void);
};
#endif

