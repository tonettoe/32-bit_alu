#ifndef SHIFTERRIGHT_HPP
#define SHIFTERRIGHT_HPP

#include <systemc.h>
#include <string>


#define NBIT 32

SC_MODULE(SHIFTERRIGHT) {

	sc_in<sc_lv<NBIT> > shifterright_A;
	sc_out<sc_lv<NBIT> > shifterright_R;

	
	SC_CTOR(SHIFTERRIGHT){
		SC_THREAD(shifterright_thread);
			sensitive << shifterright_A;
		}	
		void shifterright_thread(void);
};
#endif

