#ifndef XOR32B_HPP
#define XOR32B_HPP

#include <systemc.h>
#include <string>
#include "xor1b.hpp"
#include "conv32to1.hpp"
#include "conv1to32.hpp"

#define NBIT 32

SC_MODULE(XOR32b) {

	sc_in<sc_lv<NBIT> > xor32b_A, xor32b_B;
	sc_out<sc_lv<NBIT> > xor32b_R;

	//inner signal
	sc_signal<sc_logic> xa0,xa1,xa2,xa3,xa4,xa5,xa6,xa7,xa8,xa9,xa10,xa11,xa12,xa13,xa14,xa15,xa16,xa17,xa18,xa19,xa20,xa21,xa22,xa23,xa24,xa25,xa26,xa27,xa28,xa29,xa30,xa31;
	sc_signal<sc_logic> xb0,xb1,xb2,xb3,xb4,xb5,xb6,xb7,xb8,xb9,xb10,xb11,xb12,xb13,xb14,xb15,xb16,xb17,xb18,xb19,xb20,xb21,xb22,xb23,xb24,xb25,xb26,xb27,xb28,xb29,xb30,xb31;
	sc_signal<sc_logic> xo0,xo1,xo2,xo3,xo4,xo5,xo6,xo7,xo8,xo9,xo10,xo11,xo12,xo13,xo14,xo15,xo16,xo17,xo18,xo19,xo20,xo21,xo22,xo23,xo24,xo25,xo26,xo27,xo28,xo29,xo30,xo31;	
	sc_signal<sc_lv<NBIT> > conv1in_frominput, conv2in_frominput, convout_foroutput, xor32b_R_forout;
	
	//inner components
	XOR1b x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31;
	conv1to32 conv1in, conv2in;
	conv32to1 convout;

	SC_CTOR(XOR32b) : x0("x0"),x1("x1"),x2("x2"),x3("x3"),x4("x4"),x5("x5"),x6("x6"),x7("x7"),x8("x8"),x9("x9"),x10("x10"),x11("x11"),x12("x12"),x13("x13"),x14("x14"),x15("x15"),x16("x16"),x17("x17"),x18("x18"),x19("x19"),x20("x20"),x21("x21"),x22("x22"),x23("x23"),x24("x24"),x25("x25"),x26("x26"),x27("x27"),x28("x28"),x29("x29"),x30("x30"),x31("x31"),conv1in("conv1in"),conv2in("conv2in"),convout("convout")
				{

				x0.xor_in1(xa0);
				x0.xor_in2(xb0);
				x0.xor_out(xo0);
				
				x1.xor_in1(xa1);
				x1.xor_in2(xb1);
				x1.xor_out(xo1);

				x2.xor_in1(xa2);
				x2.xor_in2(xb2);
				x2.xor_out(xo2);
				
				x3.xor_in1(xa3);
				x3.xor_in2(xb3);
				x3.xor_out(xo3);
				
				x4.xor_in1(xa4);
				x4.xor_in2(xb4);
				x4.xor_out(xo4);
				
				x5.xor_in1(xa5);
				x5.xor_in2(xb5);
				x5.xor_out(xo5);
				
				x6.xor_in1(xa6);
				x6.xor_in2(xb6);
				x6.xor_out(xo6);
				
				x7.xor_in1(xa7);
				x7.xor_in2(xb7);
				x7.xor_out(xo7);
				
				x8.xor_in1(xa8);
				x8.xor_in2(xb8);
				x8.xor_out(xo8);
				
				x9.xor_in1(xa9);
				x9.xor_in2(xb9);
				x9.xor_out(xo9);
				
				x10.xor_in1(xa10);
				x10.xor_in2(xb10);
				x10.xor_out(xo10);
				
				x11.xor_in1(xa11);
				x11.xor_in2(xb11);
				x11.xor_out(xo11);
				
				x12.xor_in1(xa12);
				x12.xor_in2(xb12);
				x12.xor_out(xo12);
				
				x13.xor_in1(xa13);
				x13.xor_in2(xb13);
				x13.xor_out(xo13);
				
				x14.xor_in1(xa14);
				x14.xor_in2(xb14);
				x14.xor_out(xo14);
				
				x15.xor_in1(xa15);
				x15.xor_in2(xb15);
				x15.xor_out(xo15);
				
				x16.xor_in1(xa16);
				x16.xor_in2(xb16);
				x16.xor_out(xo16);
				
				x17.xor_in1(xa17);
				x17.xor_in2(xb17);
				x17.xor_out(xo17);
				
				x18.xor_in1(xa18);
				x18.xor_in2(xb18);
				x18.xor_out(xo18);
				
				x19.xor_in1(xa19);
				x19.xor_in2(xb19);
				x19.xor_out(xo19);
				// x20
				x20.xor_in1(xa20);
				x20.xor_in2(xb20);
				x20.xor_out(xo20);
				
				x21.xor_in1(xa21);
				x21.xor_in2(xb21);
				x21.xor_out(xo21);
				
				x22.xor_in1(xa22);
				x22.xor_in2(xb22);
				x22.xor_out(xo22);
				
				x23.xor_in1(xa23);
				x23.xor_in2(xb23);
				x23.xor_out(xo23);
				
				x24.xor_in1(xa24);
				x24.xor_in2(xb24);
				x24.xor_out(xo24);
				
				x25.xor_in1(xa25);
				x25.xor_in2(xb25);
				x25.xor_out(xo25);
				
				x26.xor_in1(xa26);
				x26.xor_in2(xb26);
				x26.xor_out(xo26);
				
				x27.xor_in1(xa27);
				x27.xor_in2(xb27);
				x27.xor_out(xo27);
				
				x28.xor_in1(xa28);
				x28.xor_in2(xb28);
				x28.xor_out(xo28);
				
				x29.xor_in1(xa29);
				x29.xor_in2(xb29);
				x29.xor_out(xo29);
				
				x30.xor_in1(xa30);
				x30.xor_in2(xb30);
				x30.xor_out(xo30);
				
				x31.xor_in1(xa31);
				x31.xor_in2(xb31);
				x31.xor_out(xo31);

				conv1in.conv_in(conv1in_frominput);	
				conv1in.conv0_out(xa0);
				conv1in.conv1_out(xa1);
				conv1in.conv2_out(xa2);
				conv1in.conv3_out(xa3);
				conv1in.conv4_out(xa4);
				conv1in.conv5_out(xa5);
				conv1in.conv6_out(xa6);
				conv1in.conv7_out(xa7);
				conv1in.conv8_out(xa8);
				conv1in.conv9_out(xa9);
				conv1in.conv10_out(xa10);
				conv1in.conv11_out(xa11);
				conv1in.conv12_out(xa12);
				conv1in.conv13_out(xa13);
				conv1in.conv14_out(xa14);
				conv1in.conv15_out(xa15);
				conv1in.conv16_out(xa16);
				conv1in.conv17_out(xa17);
				conv1in.conv18_out(xa18);
				conv1in.conv19_out(xa19);
				conv1in.conv20_out(xa20);
				conv1in.conv21_out(xa21);
				conv1in.conv22_out(xa22);
				conv1in.conv23_out(xa23);
				conv1in.conv24_out(xa24);
				conv1in.conv25_out(xa25);
				conv1in.conv26_out(xa26);
				conv1in.conv27_out(xa27);
				conv1in.conv28_out(xa28);
				conv1in.conv29_out(xa29);
				conv1in.conv30_out(xa30);
				conv1in.conv31_out(xa31);

				conv2in.conv_in(conv2in_frominput);
				conv2in.conv0_out(xb0);
				conv2in.conv1_out(xb1);
				conv2in.conv2_out(xb2);
				conv2in.conv3_out(xb3);
				conv2in.conv4_out(xb4);
				conv2in.conv5_out(xb5);
				conv2in.conv6_out(xb6);
				conv2in.conv7_out(xb7);
				conv2in.conv8_out(xb8);
				conv2in.conv9_out(xb9);
				conv2in.conv10_out(xb10);
				conv2in.conv11_out(xb11);
				conv2in.conv12_out(xb12);
				conv2in.conv13_out(xb13);
				conv2in.conv14_out(xb14);
				conv2in.conv15_out(xb15);
				conv2in.conv16_out(xb16);
				conv2in.conv17_out(xb17);
				conv2in.conv18_out(xb18);
				conv2in.conv19_out(xb19);
				conv2in.conv20_out(xb20);
				conv2in.conv21_out(xb21);
				conv2in.conv22_out(xb22);
				conv2in.conv23_out(xb23);
				conv2in.conv24_out(xb24);
				conv2in.conv25_out(xb25);
				conv2in.conv26_out(xb26);
				conv2in.conv27_out(xb27);
				conv2in.conv28_out(xb28);
				conv2in.conv29_out(xb29);
				conv2in.conv30_out(xb30);
				conv2in.conv31_out(xb31);

				convout.conv_out(xor32b_R_forout);
				convout.conv0_in(xo0);
				convout.conv1_in(xo1);
				convout.conv2_in(xo2);
				convout.conv3_in(xo3);
				convout.conv4_in(xo4);
				convout.conv5_in(xo5);
				convout.conv6_in(xo6);
				convout.conv7_in(xo7);
				convout.conv8_in(xo8);
				convout.conv9_in(xo9);
				convout.conv10_in(xo10);
				convout.conv11_in(xo11);
				convout.conv12_in(xo12);
				convout.conv13_in(xo13);
				convout.conv14_in(xo14);
				convout.conv15_in(xo15);
				convout.conv16_in(xo16);
				convout.conv17_in(xo17);
				convout.conv18_in(xo18);
				convout.conv19_in(xo19);
				convout.conv20_in(xo20);
				convout.conv21_in(xo21);
				convout.conv22_in(xo22);
				convout.conv23_in(xo23);
				convout.conv24_in(xo24);
				convout.conv25_in(xo25);
				convout.conv26_in(xo26);
				convout.conv27_in(xo27);
				convout.conv28_in(xo28);
				convout.conv29_in(xo29);
				convout.conv30_in(xo30);
				convout.conv31_in(xo31);

				SC_THREAD(xor32b_thread);
					sensitive << xor32b_A << xor32b_B;
				;
			}
		void xor32b_thread(void);
};
#endif

