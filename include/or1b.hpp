#ifndef OR1B_HPP
#define OR1B_HPP

SC_MODULE(OR1b) { 

	sc_in<sc_logic> or_in1;
	sc_in<sc_logic> or_in2;
	sc_out<sc_logic> or_out;

	SC_CTOR(OR1b){
		SC_THREAD(or1b_thread);
		sensitive << or_in1
			  << or_in2;
	}
	
	void or1b_thread();
};

#endif
