#ifndef NOT1B_HPP
#define NOT1B_HPP

SC_MODULE(NOT1b) {

	sc_in<sc_logic> not_in;
	sc_out<sc_logic> not_out;

	SC_CTOR(NOT1b){
		SC_THREAD(not1b_thread);
		sensitive << not_in;
	}
		
	void not1b_thread();
};

#endif
