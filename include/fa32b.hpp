#ifndef FA32B_HPP
#define FA32B_HPP

#include <systemc.h>
#include <string>
#include "fa1b.hpp"
#include "conv1to32.hpp"
#include "conv32to1.hpp"

#define NBIT 32

SC_MODULE(FA32b) {	

	//Input//
	sc_in<sc_lv<NBIT> > 	fa32b_A_in; 
	sc_in<sc_lv<NBIT> > 	fa32b_B_in;
	sc_in<sc_logic> 	fa32b_c0_in;
	
	//Output//
	sc_out<sc_lv<NBIT> > 	fa32b_R_out;
	sc_out<sc_logic> 	fa32b_c32_out;


	///INNER SIGNAL///

	//carry
	sc_signal<sc_logic> c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22,c23,c24,c25,c26,c27,c28,c29,c30,c31,c32;;

	//signals for the "a" input of single fa1b [1bit full adder]
	sc_signal<sc_logic> sa0,sa1,sa2,sa3,sa4,sa5,sa6,sa7,sa8,sa9,sa10,sa11,sa12,sa13,sa14,sa15,sa16,sa17,sa18,sa19,sa20,sa21,sa22,sa23,sa24,sa25,sa26,sa27,sa28,sa29,sa30,sa31;

	//signals for the "b" input of single fa1b[1bit full adder]	
	sc_signal<sc_logic> sb0,sb1,sb2,sb3,sb4,sb5,sb6,sb7,sb8,sb9,sb10,sb11,sb12,sb13,sb14,sb15,sb16,sb17,sb18,sb19,sb20,sb21,sb22,sb23,sb24,sb25,sb26,sb27,sb28,sb29,sb30,sb31;
	
	//signals for the result output of single fa1b[1bit full adder]	
	sc_signal<sc_logic> r0,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15,r16,r17,r18,r19,r20,r21,r22,r23,r24,r25,r26,r27,r28,r29,r30,r31;	

	//signals for input anc output converters
	sc_signal<sc_lv<NBIT> > conv1in_frominput, conv2in_frominput;
	sc_signal<sc_lv<NBIT> > fa32b_R;	

				
	//INNER COMPONENTS//

	FA1b f0,f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11,f12,f13,f14,f15,f16,f17,f18,f19,f20,f21,f22,f23,f24,f25,f26,f27,f28,f29,f30,f31;
	conv1to32 conv1in,conv2in;
	conv32to1 conv1out;

	SC_CTOR(FA32b) :f0("f0"),f1("f1"),f2("f2"),f3("f3"),f4("f4"),f5("f5"),f6("f6"),f7("f7"),f8("f8"),f9("f9"),f10("f10"),f11("f11"),f12("f12"),f13("f13"),f14("f14"),f15("f15"),f16("f16"),
			f17("f17"),f18("f18"),f19("f19"),f20("f20"),f21("f21"),f22("f22"),f23("f23"),f24("f24"),f25("f25"),f26("f26"),f27("f27"),f28("f28"),f29("f29"),f30("f30"),f31("f31"),
			conv1in("conv1in"),conv2in("conv2in"),conv1out("conv1out"){


		f0.fa1b_a(sa0);
		f0.fa1b_b(sb0);
		f0.fa1b_carryin(c0);
		f0.fa1b_carryout(c1);
		f0.fa1b_res(r0);
			
		f1.fa1b_a(sa1);
		f1.fa1b_b(sb1);
		f1.fa1b_carryin(c1);
		f1.fa1b_carryout(c2);
		f1.fa1b_res(r1);

		f2.fa1b_a(sa2);
		f2.fa1b_b(sb2);
		f2.fa1b_carryin(c2);
		f2.fa1b_carryout(c3);
		f2.fa1b_res(r2);

		f3.fa1b_a(sa3);
		f3.fa1b_b(sb3);
		f3.fa1b_carryin(c3);
		f3.fa1b_carryout(c4);
		f3.fa1b_res(r3);

		f4.fa1b_a(sa4);
		f4.fa1b_b(sb4);
		f4.fa1b_carryin(c4);
		f4.fa1b_carryout(c5);
		f4.fa1b_res(r4);

		f5.fa1b_a(sa5);
		f5.fa1b_b(sb5);
		f5.fa1b_carryin(c5);
		f5.fa1b_carryout(c6);
		f5.fa1b_res(r5);

		f6.fa1b_a(sa6);
		f6.fa1b_b(sb6);
		f6.fa1b_carryin(c6);
		f6.fa1b_carryout(c7);
		f6.fa1b_res(r6);

		f7.fa1b_a(sa7);
		f7.fa1b_b(sb7);
		f7.fa1b_carryin(c7);
		f7.fa1b_carryout(c8);
		f7.fa1b_res(r7);

		f8.fa1b_a(sa8);
		f8.fa1b_b(sb8);
		f8.fa1b_carryin(c8);
		f8.fa1b_carryout(c9);
		f8.fa1b_res(r8);

		f9.fa1b_a(sa9);
		f9.fa1b_b(sb9);
		f9.fa1b_carryin(c9);
		f9.fa1b_carryout(c10);
		f9.fa1b_res(r9);

		f10.fa1b_a(sa10);
		f10.fa1b_b(sb10);
		f10.fa1b_carryin(c10);
		f10.fa1b_carryout(c11);
		f10.fa1b_res(r10);

		f11.fa1b_a(sa11);
		f11.fa1b_b(sb11);
		f11.fa1b_carryin(c11);
		f11.fa1b_carryout(c12);
		f11.fa1b_res(r11);

		f12.fa1b_a(sa12);
		f12.fa1b_b(sb12);
		f12.fa1b_carryin(c12);
		f12.fa1b_carryout(c13);
		f12.fa1b_res(r12);

		f13.fa1b_a(sa13);
		f13.fa1b_b(sb13);
		f13.fa1b_carryin(c13);
		f13.fa1b_carryout(c14);
		f13.fa1b_res(r13);

		f14.fa1b_a(sa14);
		f14.fa1b_b(sb14);
		f14.fa1b_carryin(c14);
		f14.fa1b_carryout(c15);
		f14.fa1b_res(r14);

		f15.fa1b_a(sa15);
		f15.fa1b_b(sb15);
		f15.fa1b_carryin(c15);
		f15.fa1b_carryout(c16);
		f15.fa1b_res(r15);

		f16.fa1b_a(sa16);
		f16.fa1b_b(sb16);
		f16.fa1b_carryin(c16);
		f16.fa1b_carryout(c17);
		f16.fa1b_res(r16);

		f17.fa1b_a(sa17);
		f17.fa1b_b(sb17);
		f17.fa1b_carryin(c17);
		f17.fa1b_carryout(c18);
		f17.fa1b_res(r17);

		f18.fa1b_a(sa18);
		f18.fa1b_b(sb18);
		f18.fa1b_carryin(c18);
		f18.fa1b_carryout(c19);
		f18.fa1b_res(r18);

		f19.fa1b_a(sa19);
		f19.fa1b_b(sb19);
		f19.fa1b_carryin(c19);
		f19.fa1b_carryout(c20);
		f19.fa1b_res(r19);

		f20.fa1b_a(sa20);
		f20.fa1b_b(sb20);
		f20.fa1b_carryin(c20);
		f20.fa1b_carryout(c21);
		f20.fa1b_res(r20);

		f21.fa1b_a(sa21);
		f21.fa1b_b(sb21);
		f21.fa1b_carryin(c21);
		f21.fa1b_carryout(c22);
		f21.fa1b_res(r21);

		f22.fa1b_a(sa22);
		f22.fa1b_b(sb22);
		f22.fa1b_carryin(c22);
		f22.fa1b_carryout(c23);
		f22.fa1b_res(r22);

		f23.fa1b_a(sa23);
		f23.fa1b_b(sb23);
		f23.fa1b_carryin(c23);
		f23.fa1b_carryout(c24);
		f23.fa1b_res(r23);

		f24.fa1b_a(sa24);
		f24.fa1b_b(sb24);
		f24.fa1b_carryin(c24);
		f24.fa1b_carryout(c25);
		f24.fa1b_res(r24);

		f25.fa1b_a(sa25);
		f25.fa1b_b(sb25);
		f25.fa1b_carryin(c25);
		f25.fa1b_carryout(c26);
		f25.fa1b_res(r25);

		f26.fa1b_a(sa26);
		f26.fa1b_b(sb26);
		f26.fa1b_carryin(c26);
		f26.fa1b_carryout(c27);
		f26.fa1b_res(r26);

		f27.fa1b_a(sa27);
		f27.fa1b_b(sb27);
		f27.fa1b_carryin(c27);
		f27.fa1b_carryout(c28);
		f27.fa1b_res(r27);

		f28.fa1b_a(sa28);
		f28.fa1b_b(sb28);
		f28.fa1b_carryin(c28);
		f28.fa1b_carryout(c29);
		f28.fa1b_res(r28);

		f29.fa1b_a(sa29);
		f29.fa1b_b(sb29);
		f29.fa1b_carryin(c29);
		f29.fa1b_carryout(c30);
		f29.fa1b_res(r29);

		f30.fa1b_a(sa30);
		f30.fa1b_b(sb30);
		f30.fa1b_carryin(c30);
		f30.fa1b_carryout(c31);
		f30.fa1b_res(r30);

		f31.fa1b_a(sa31);
		f31.fa1b_b(sb31);
		f31.fa1b_carryin(c31);
		f31.fa1b_carryout(c32);
		f31.fa1b_res(r31);

		conv1in.conv_in(conv1in_frominput);
		conv1in.conv0_out(sa0);
		conv1in.conv1_out(sa1);
		conv1in.conv2_out(sa2);
		conv1in.conv3_out(sa3);
		conv1in.conv4_out(sa4);
		conv1in.conv5_out(sa5);
		conv1in.conv6_out(sa6);
		conv1in.conv7_out(sa7);
		conv1in.conv8_out(sa8);
		conv1in.conv9_out(sa9);
		conv1in.conv10_out(sa10);
		conv1in.conv11_out(sa11);
		conv1in.conv12_out(sa12);
		conv1in.conv13_out(sa13);
		conv1in.conv14_out(sa14);
		conv1in.conv15_out(sa15);
		conv1in.conv16_out(sa16);
		conv1in.conv17_out(sa17);
		conv1in.conv18_out(sa18);
		conv1in.conv19_out(sa19);
		conv1in.conv20_out(sa20);
		conv1in.conv21_out(sa21);
		conv1in.conv22_out(sa22);
		conv1in.conv23_out(sa23);
		conv1in.conv24_out(sa24);
		conv1in.conv25_out(sa25);
		conv1in.conv26_out(sa26);
		conv1in.conv27_out(sa27);
		conv1in.conv28_out(sa28);
		conv1in.conv29_out(sa29);
		conv1in.conv30_out(sa30);
		conv1in.conv31_out(sa31);

		conv2in.conv_in(conv2in_frominput);
		conv2in.conv0_out(sb0);
		conv2in.conv1_out(sb1);
		conv2in.conv2_out(sb2);
		conv2in.conv3_out(sb3);
		conv2in.conv4_out(sb4);
		conv2in.conv5_out(sb5);
		conv2in.conv6_out(sb6);
		conv2in.conv7_out(sb7);
		conv2in.conv8_out(sb8);
		conv2in.conv9_out(sb9);
		conv2in.conv10_out(sb10);
		conv2in.conv11_out(sb11);
		conv2in.conv12_out(sb12);
		conv2in.conv13_out(sb13);
		conv2in.conv14_out(sb14);
		conv2in.conv15_out(sb15);
		conv2in.conv16_out(sb16);
		conv2in.conv17_out(sb17);
		conv2in.conv18_out(sb18);
		conv2in.conv19_out(sb19);
		conv2in.conv20_out(sb20);
		conv2in.conv21_out(sb21);
		conv2in.conv22_out(sb22);
		conv2in.conv23_out(sb23);
		conv2in.conv24_out(sb24);
		conv2in.conv25_out(sb25);
		conv2in.conv26_out(sb26);
		conv2in.conv27_out(sb27);
		conv2in.conv28_out(sb28);
		conv2in.conv29_out(sb29);
		conv2in.conv30_out(sb30);
		conv2in.conv31_out(sb31);

		conv1out.conv_out(fa32b_R);
		conv1out.conv0_in(r0);
		conv1out.conv1_in(r1);
		conv1out.conv2_in(r2);
		conv1out.conv3_in(r3);
		conv1out.conv4_in(r4);
		conv1out.conv5_in(r5);
		conv1out.conv6_in(r6);
		conv1out.conv7_in(r7);
		conv1out.conv8_in(r8);
		conv1out.conv9_in(r9);
		conv1out.conv10_in(r10);
		conv1out.conv11_in(r11);
		conv1out.conv12_in(r12);
		conv1out.conv13_in(r13);
		conv1out.conv14_in(r14);
		conv1out.conv15_in(r15);
		conv1out.conv16_in(r16);
		conv1out.conv17_in(r17);
		conv1out.conv18_in(r18);
		conv1out.conv19_in(r19);
		conv1out.conv20_in(r20);
		conv1out.conv21_in(r21);
		conv1out.conv22_in(r22);
		conv1out.conv23_in(r23);
		conv1out.conv24_in(r24);
		conv1out.conv25_in(r25);
		conv1out.conv26_in(r26);
		conv1out.conv27_in(r27);
		conv1out.conv28_in(r28);
		conv1out.conv29_in(r29);
		conv1out.conv30_in(r30);
		conv1out.conv31_in(r31);


		SC_THREAD(fa32b_thread);
			sensitive << fa32b_A_in << fa32b_B_in << fa32b_c0_in;
		;				
	}
	
	void fa32b_thread(void);
};
#endif

