#ifndef AND1B_HPP
#define AND1B_HPP

SC_MODULE(AND1b) { 

	sc_in <sc_logic> and_in1;
	sc_in <sc_logic> and_in2;
	sc_out<sc_logic> and_out;

	SC_CTOR(AND1b){
		SC_THREAD(and1b_thread);
		sensitive << and_in1 
			  << and_in2;
	}
	void and1b_thread();
};

#endif
