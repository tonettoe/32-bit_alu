//	ALU component with 3 input and 2 output
//	Input A and input B 32bit logic vector are the data input
//	Input OP 3bit logic vector is the operation input
//	RES is the 32bit logic vector result output
//	CARRY is the 1bit output result 


#ifndef ALU_HPP
#define ALU_HPP

#include <systemc.h>
#include <string>

#include "not32b.hpp"
#include "and32b.hpp"
#include "or32b.hpp"
#include "xor32b.hpp"
#include "shifterright.hpp"
#include "shifterleft.hpp"
#include "fa32b.hpp"
#include "mux8x32.hpp"

#define NBIT 32
#define NOP 3

SC_MODULE(ALU) { 
	
	sc_in <sc_lv<NBIT> > A;
	sc_in <sc_lv<NBIT> > B;
	sc_out<sc_lv<NOP> > OP;
	sc_out<sc_lv<NBIT> > RES;
	sc_out<sc_logic> CARRY;

	//inner signal
	sc_signal<sc_lv<NBIT> >Ain,Bin,RESout,outputnot,outputand,outputor,outputxor,outputright,outputleft,outputadder;
	sc_signal<sc_lv<NOP> > OPin;
	sc_signal<sc_logic> CARRYout,CARRYin;
	sc_logic zero;
	
	//inner components
	NOT32b notcomponent;
	AND32b andcomponent;
	OR32b orcomponent;
	XOR32b xorcomponent;
	SHIFTERRIGHT srcomponent;
	SHIFTERLEFT slcomponent;
	FA32b	addercomponent;
	MUX8x32 muxcomponent;


	SC_CTOR(ALU) : notcomponent("notcomponent"),andcomponent("andcomponent"),orcomponent("orcomponent"),
			xorcomponent("xorcomponent"),srcomponent("srcomponent"),slcomponent("slcomponent"),
			addercomponent("addercomponent"),muxcomponent("muxcomponent")
		{
		
		notcomponent.not32b_A(Ain);
		notcomponent.not32b_R(outputnot);
		
		andcomponent.and32b_A(Ain);
		andcomponent.and32b_B(Bin);		
		andcomponent.and32b_R(outputand);

		orcomponent.or32b_A_in(Ain);
		orcomponent.or32b_B_in(Bin);
		orcomponent.or32b_R_out(outputor);

		xorcomponent.xor32b_A(Ain);
		xorcomponent.xor32b_B(Bin);
		xorcomponent.xor32b_R(outputxor);

		srcomponent.shifterright_A(Ain);
		srcomponent.shifterright_R(outputright);		

		slcomponent.shifterleft_A(Ain);
		slcomponent.shifterleft_R(outputleft);

		addercomponent.fa32b_A_in(Ain);		
		addercomponent.fa32b_B_in(Bin);
		addercomponent.fa32b_c0_in(CARRYin);
		addercomponent.fa32b_R_out(outputadder);
		addercomponent.fa32b_c32_out(CARRYout);

		muxcomponent.mux8x32_sel(OPin);
		muxcomponent.mux8x32_0(Ain);
		muxcomponent.mux8x32_1(outputnot);
		muxcomponent.mux8x32_2(outputand);
		muxcomponent.mux8x32_3(outputor);
		muxcomponent.mux8x32_4(outputxor);
		muxcomponent.mux8x32_5(outputright);
		muxcomponent.mux8x32_6(outputleft);
		muxcomponent.mux8x32_7(outputadder);		
		muxcomponent.mux8x32_OUT(RESout);		

		SC_THREAD(alu_thread);
		sensitive << A 
			  << B;
	}
	void alu_thread();
};

#endif
