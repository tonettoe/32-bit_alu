#ifndef MUX8X32_HPP
#define MUX8X32_HPP

#include <systemc.h>
#include <string>

#define NBIT 32
#define NSEL 3

SC_MODULE(MUX8x32) {

	sc_in<sc_lv<NSEL> > mux8x32_sel;
	sc_in<sc_lv<NBIT> > mux8x32_0, mux8x32_1, mux8x32_2, mux8x32_3, mux8x32_4, mux8x32_5, mux8x32_6, mux8x32_7;
	sc_out<sc_lv<NBIT> > mux8x32_OUT;


	SC_CTOR(MUX8x32){
		SC_THREAD(mux8x32_thread);
			sensitive << mux8x32_0 << mux8x32_1 << mux8x32_2 << mux8x32_3 << mux8x32_4 << mux8x32_5 <<  mux8x32_6 << mux8x32_7 << mux8x32_sel;
		
	}

	void mux8x32_thread(void);
};
#endif
		

