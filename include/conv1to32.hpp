#ifndef CONV1TO32_HPP
#define CONV1TO32_HPP

#include <systemc.h>
#include <string>
#include "conv1to32.hpp"

#define NBIT 32

SC_MODULE(conv1to32) {	

	//Input//
	sc_in<sc_lv<NBIT> > 	conv_in; 
	
	//Output//

	sc_out<sc_logic> 	conv0_out;
	sc_out<sc_logic> 	conv1_out;
	sc_out<sc_logic> 	conv2_out;
	sc_out<sc_logic> 	conv3_out;
	sc_out<sc_logic> 	conv4_out;
	sc_out<sc_logic> 	conv5_out;
	sc_out<sc_logic> 	conv6_out;
	sc_out<sc_logic> 	conv7_out;
	sc_out<sc_logic> 	conv8_out;
	sc_out<sc_logic> 	conv9_out;
	sc_out<sc_logic> 	conv10_out;
	sc_out<sc_logic> 	conv11_out;
	sc_out<sc_logic> 	conv12_out;
	sc_out<sc_logic> 	conv13_out;
	sc_out<sc_logic> 	conv14_out;
	sc_out<sc_logic> 	conv15_out;
	sc_out<sc_logic> 	conv16_out;
	sc_out<sc_logic> 	conv17_out;
	sc_out<sc_logic> 	conv18_out;
	sc_out<sc_logic> 	conv19_out;
	sc_out<sc_logic> 	conv20_out;
	sc_out<sc_logic> 	conv21_out;
	sc_out<sc_logic> 	conv22_out;
	sc_out<sc_logic> 	conv23_out;
	sc_out<sc_logic> 	conv24_out;
	sc_out<sc_logic> 	conv25_out;
	sc_out<sc_logic> 	conv26_out;
	sc_out<sc_logic> 	conv27_out;
	sc_out<sc_logic> 	conv28_out;
	sc_out<sc_logic> 	conv29_out;
	sc_out<sc_logic> 	conv30_out;
	sc_out<sc_logic> 	conv31_out;

	///INNER SIGNAL///

	sc_lv<NBIT> A_in;

	sc_signal<sc_logic> c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22,c23,c24,c25,c26,c27,c28,c29,c30,c31;

	SC_CTOR(conv1to32){

		SC_THREAD(conv1to32_thread);
			sensitive << conv_in;
		;				
	}
	void conv1to32_thread(void);
};
#endif
	
