#ifndef AND32B_HPP
#define AND32B_HPP

#include <systemc.h>
#include <string>
#include "and1b.hpp"
#include "conv32to1.hpp"
#include "conv1to32.hpp"

#define NBIT 32

SC_MODULE(AND32b) {

	sc_in<sc_lv<NBIT> > and32b_A, and32b_B;
	sc_out<sc_lv<NBIT> > and32b_R;
	
	sc_signal<sc_logic> aa0,aa1,aa2,aa3,aa4,aa5,aa6,aa7,aa8,aa9,aa10,aa11,aa12,aa13,aa14,aa15,aa16,aa17,aa18,aa19,aa20,aa21,aa22,aa23,aa24,aa25,aa26,aa27,aa28,aa29,aa30,aa31;
	sc_signal<sc_logic> ab0,ab1,ab2,ab3,ab4,ab5,ab6,ab7,ab8,ab9,ab10,ab11,ab12,ab13,ab14,ab15,ab16,ab17,ab18,ab19,ab20,ab21,ab22,ab23,ab24,ab25,ab26,ab27,ab28,ab29,ab30,ab31;
	sc_signal<sc_logic> ao0,ao1,ao2,ao3,ao4,ao5,ao6,ao7,ao8,ao9,ao10,ao11,ao12,ao13,ao14,ao15,ao16,ao17,ao18,ao19,ao20,ao21,ao22,ao23,ao24,ao25,ao26,ao27,ao28,ao29,ao30,ao31;	
	sc_signal<sc_lv<NBIT> > conv1in_frominput, conv2in_frominput, and32b_R_forout;

	//inner components
	AND1b a0,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18,a19,a20,a21,a22,a23,a24,a25,a26,a27,a28,a29,a30,a31;
	conv1to32 conv1in, conv2in;
	conv32to1 convout;
	SC_CTOR(AND32b) : a0("a0"),a1("a1"),a2("a2"),a3("a3"),a4("a4"),a5("a5"),a6("a6"),a7("a7"),a8("a8"),a9("a9"),a10("a10"),a11("a11"),a12("a12"),a13("a13"),a14("a14"),a15("a15"),a16("a16"),a17("a17"),a18("a18"),a19("a19"),a20("a20"),a21("a21"),a22("a22"),a23("a23"),a24("a24"),a25("a25"),a26("a26"),a27("a27"),a28("a28"),a29("a29"),a30("a30"),a31("a31"),conv1in("conv1in"),conv2in("conv2in"),convout("convout")
				{

				a0.and_in1(aa0);
				a0.and_in2(ab0);
				a0.and_out(ao0);
				
				a1.and_in1(aa1);
				a1.and_in2(ab1);
				a1.and_out(ao1);

				a2.and_in1(aa2);
				a2.and_in2(ab2);
				a2.and_out(ao2);
				
				a3.and_in1(aa3);
				a3.and_in2(ab3);
				a3.and_out(ao3);
				
				a4.and_in1(aa4);
				a4.and_in2(ab4);
				a4.and_out(ao4);
				
				a5.and_in1(aa5);
				a5.and_in2(ab5);
				a5.and_out(ao5);
				
				a6.and_in1(aa6);
				a6.and_in2(ab6);
				a6.and_out(ao6);
				
				a7.and_in1(aa7);
				a7.and_in2(ab7);
				a7.and_out(ao7);
				
				a8.and_in1(aa8);
				a8.and_in2(ab8);
				a8.and_out(ao8);
				
				a9.and_in1(aa9);
				a9.and_in2(ab9);
				a9.and_out(ao9);
				
				a10.and_in1(aa10);
				a10.and_in2(ab10);
				a10.and_out(ao10);
				
				a11.and_in1(aa11);
				a11.and_in2(ab11);
				a11.and_out(ao11);
				
				a12.and_in1(aa12);
				a12.and_in2(ab12);
				a12.and_out(ao12);
				
				a13.and_in1(aa13);
				a13.and_in2(ab13);
				a13.and_out(ao13);
				
				a14.and_in1(aa14);
				a14.and_in2(ab14);
				a14.and_out(ao14);
				
				a15.and_in1(aa15);
				a15.and_in2(ab15);
				a15.and_out(ao15);
				
				a16.and_in1(aa16);
				a16.and_in2(ab16);
				a16.and_out(ao16);
				
				a17.and_in1(aa17);
				a17.and_in2(ab17);
				a17.and_out(ao17);
				
				a18.and_in1(aa18);
				a18.and_in2(ab18);
				a18.and_out(ao18);
				
				a19.and_in1(aa19);
				a19.and_in2(ab19);
				a19.and_out(ao19);

				a20.and_in1(aa20);
				a20.and_in2(ab20);
				a20.and_out(ao20);
				
				a21.and_in1(aa21);
				a21.and_in2(ab21);
				a21.and_out(ao21);
				
				a22.and_in1(aa22);
				a22.and_in2(ab22);
				a22.and_out(ao22);
				
				a23.and_in1(aa23);
				a23.and_in2(ab23);
				a23.and_out(ao23);
				
				a24.and_in1(aa24);
				a24.and_in2(ab24);
				a24.and_out(ao24);
				
				a25.and_in1(aa25);
				a25.and_in2(ab25);
				a25.and_out(ao25);
				
				a26.and_in1(aa26);
				a26.and_in2(ab26);
				a26.and_out(ao26);
				
				a27.and_in1(aa27);
				a27.and_in2(ab27);
				a27.and_out(ao27);
				
				a28.and_in1(aa28);
				a28.and_in2(ab28);
				a28.and_out(ao28);
				
				a29.and_in1(aa29);
				a29.and_in2(ab29);
				a29.and_out(ao29);
				
				a30.and_in1(aa30);
				a30.and_in2(ab30);
				a30.and_out(ao30);
				
				a31.and_in1(aa31);
				a31.and_in2(ab31);
				a31.and_out(ao31);

				conv1in.conv_in(conv1in_frominput);	
				conv1in.conv0_out(aa0);
				conv1in.conv1_out(aa1);
				conv1in.conv2_out(aa2);
				conv1in.conv3_out(aa3);
				conv1in.conv4_out(aa4);
				conv1in.conv5_out(aa5);
				conv1in.conv6_out(aa6);
				conv1in.conv7_out(aa7);
				conv1in.conv8_out(aa8);
				conv1in.conv9_out(aa9);
				conv1in.conv10_out(aa10);
				conv1in.conv11_out(aa11);
				conv1in.conv12_out(aa12);
				conv1in.conv13_out(aa13);
				conv1in.conv14_out(aa14);
				conv1in.conv15_out(aa15);
				conv1in.conv16_out(aa16);
				conv1in.conv17_out(aa17);
				conv1in.conv18_out(aa18);
				conv1in.conv19_out(aa19);
				conv1in.conv20_out(aa20);
				conv1in.conv21_out(aa21);
				conv1in.conv22_out(aa22);
				conv1in.conv23_out(aa23);
				conv1in.conv24_out(aa24);
				conv1in.conv25_out(aa25);
				conv1in.conv26_out(aa26);
				conv1in.conv27_out(aa27);
				conv1in.conv28_out(aa28);
				conv1in.conv29_out(aa29);
				conv1in.conv30_out(aa30);
				conv1in.conv31_out(aa31);

				conv2in.conv_in(conv2in_frominput);
				conv2in.conv0_out(ab0);
				conv2in.conv1_out(ab1);
				conv2in.conv2_out(ab2);
				conv2in.conv3_out(ab3);
				conv2in.conv4_out(ab4);
				conv2in.conv5_out(ab5);
				conv2in.conv6_out(ab6);
				conv2in.conv7_out(ab7);
				conv2in.conv8_out(ab8);
				conv2in.conv9_out(ab9);
				conv2in.conv10_out(ab10);
				conv2in.conv11_out(ab11);
				conv2in.conv12_out(ab12);
				conv2in.conv13_out(ab13);
				conv2in.conv14_out(ab14);
				conv2in.conv15_out(ab15);
				conv2in.conv16_out(ab16);
				conv2in.conv17_out(ab17);
				conv2in.conv18_out(ab18);
				conv2in.conv19_out(ab19);
				conv2in.conv20_out(ab20);
				conv2in.conv21_out(ab21);
				conv2in.conv22_out(ab22);
				conv2in.conv23_out(ab23);
				conv2in.conv24_out(ab24);
				conv2in.conv25_out(ab25);
				conv2in.conv26_out(ab26);
				conv2in.conv27_out(ab27);
				conv2in.conv28_out(ab28);
				conv2in.conv29_out(ab29);
				conv2in.conv30_out(ab30);
				conv2in.conv31_out(ab31);

				convout.conv_out(and32b_R_forout);
				convout.conv0_in(ao0);
				convout.conv1_in(ao1);
				convout.conv2_in(ao2);
				convout.conv3_in(ao3);
				convout.conv4_in(ao4);
				convout.conv5_in(ao5);
				convout.conv6_in(ao6);
				convout.conv7_in(ao7);
				convout.conv8_in(ao8);
				convout.conv9_in(ao9);
				convout.conv10_in(ao10);
				convout.conv11_in(ao11);
				convout.conv12_in(ao12);
				convout.conv13_in(ao13);
				convout.conv14_in(ao14);
				convout.conv15_in(ao15);
				convout.conv16_in(ao16);
				convout.conv17_in(ao17);
				convout.conv18_in(ao18);
				convout.conv19_in(ao19);
				convout.conv20_in(ao20);
				convout.conv21_in(ao21);
				convout.conv22_in(ao22);
				convout.conv23_in(ao23);
				convout.conv24_in(ao24);
				convout.conv25_in(ao25);
				convout.conv26_in(ao26);
				convout.conv27_in(ao27);
				convout.conv28_in(ao28);
				convout.conv29_in(ao29);
				convout.conv30_in(ao30);
				convout.conv31_in(ao31);
		
				SC_THREAD(and32b_thread);
					sensitive << and32b_A << and32b_B;
				;
			}
	
		void and32b_thread(void);
};
#endif

