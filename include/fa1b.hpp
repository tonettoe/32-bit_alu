#ifndef FA1B_HPP
#define FA1B_HPP

#include <systemc.h>
#include "and1b.hpp"
#include "xor1b.hpp"
#include "or1b.hpp"

SC_MODULE(FA1b) {
	
	sc_in<sc_logic>  fa1b_a, fa1b_b, fa1b_carryin;
	sc_out<sc_logic> fa1b_res, fa1b_carryout;

	//inner signal
	sc_signal<sc_logic> xor_a_out, and_a_out, and_b_out;	

	//inner components
	XOR1b xor_a, xor_b;
	AND1b and_a, and_b;
	OR1b or_a;		

	SC_CTOR(FA1b) : xor_a("xor_a"),xor_b("xor_b"), and_a("and_a"), and_b("and_b"), 	or_a("or_a")	{

		//gate connections
		xor_a.xor_in1(fa1b_a);
		xor_a.xor_in2(fa1b_b);
		xor_a.xor_out(xor_a_out);

		xor_b.xor_in1(fa1b_carryin);
		xor_b.xor_in2(xor_a_out);
		xor_b.xor_out(fa1b_res);

		and_a.and_in1(fa1b_a);
		and_a.and_in2(fa1b_b);
		and_a.and_out(and_a_out);

		and_b.and_in1(fa1b_carryin);
		and_b.and_in2(xor_a_out);
		and_b.and_out(and_b_out);

		or_a.or_in1(and_a_out);
		or_a.or_in2(and_b_out);
		or_a.or_out(fa1b_carryout);		
	}

};

#endif
