#ifndef CONV32TO1_HPP
#define CONV32TO1_HPP

#include <systemc.h>
#include <string>
#include "conv32to1.hpp"

#define NBIT 32

SC_MODULE(conv32to1) {	

	//Output//
	sc_out<sc_lv<NBIT> > 	conv_out; 
	
	//input//

	sc_in<sc_logic>	conv0_in;
	sc_in<sc_logic>	conv1_in;
	sc_in<sc_logic>	conv2_in;
	sc_in<sc_logic>	conv3_in;
	sc_in<sc_logic>	conv4_in;
	sc_in<sc_logic>	conv5_in;
	sc_in<sc_logic>	conv6_in;
	sc_in<sc_logic>	conv7_in;
	sc_in<sc_logic>	conv8_in;
	sc_in<sc_logic>	conv9_in;
	sc_in<sc_logic>	conv10_in;
	sc_in<sc_logic>	conv11_in;
	sc_in<sc_logic>	conv12_in;
	sc_in<sc_logic>	conv13_in;
	sc_in<sc_logic>	conv14_in;
	sc_in<sc_logic>	conv15_in;
	sc_in<sc_logic>	conv16_in;
	sc_in<sc_logic>	conv17_in;
	sc_in<sc_logic>	conv18_in;
	sc_in<sc_logic>	conv19_in;
	sc_in<sc_logic>	conv20_in;
	sc_in<sc_logic>	conv21_in;
	sc_in<sc_logic>	conv22_in;
	sc_in<sc_logic>	conv23_in;
	sc_in<sc_logic>	conv24_in;
	sc_in<sc_logic>	conv25_in;
	sc_in<sc_logic>	conv26_in;
	sc_in<sc_logic>	conv27_in;
	sc_in<sc_logic>	conv28_in;
	sc_in<sc_logic>	conv29_in;
	sc_in<sc_logic>	conv30_in;
	sc_in<sc_logic>	conv31_in;
	///INNER SIGNAL///

	sc_signal<sc_logic> c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22,c23,c24,c25,c26,c27,c28,c29,c30,c31;

	sc_lv<NBIT> A_out;

	SC_CTOR(conv32to1){

		SC_THREAD(conv32to1_thread);
			sensitive << conv0_in
				  << conv1_in
				  << conv2_in
				  << conv3_in
				  << conv4_in
				  << conv5_in
				  << conv6_in
				  << conv7_in
				  << conv8_in
				  << conv9_in
				  << conv10_in
				  << conv11_in
				  << conv12_in
				  << conv13_in
				  << conv14_in
				  << conv15_in
				  << conv16_in
				  << conv17_in
				  << conv18_in
				  << conv19_in
				  << conv20_in
				  << conv21_in
				  << conv22_in
				  << conv23_in
				  << conv24_in
				  << conv25_in
				  << conv26_in
				  << conv27_in
				  << conv28_in
				  << conv29_in
				  << conv30_in
				  << conv31_in;
	
		;				
	}
	void conv32to1_thread(void);
};
#endif
	
