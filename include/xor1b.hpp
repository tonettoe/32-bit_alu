#ifndef XOR1B_HPP
#define XOR1B_HPP

SC_MODULE(XOR1b) { 

	sc_in <sc_logic> xor_in1;
	sc_in <sc_logic> xor_in2;
	sc_out<sc_logic> xor_out;

	SC_CTOR(XOR1b){
		SC_THREAD(xor1b_thread);
		sensitive << xor_in1
			  << xor_in2;
	}
	
	void xor1b_thread();
};

#endif
