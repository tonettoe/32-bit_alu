#include <systemc.h>
#include <string>
#include "mux32b.hpp"

using namespace std;

SC_MODULE(TestBench){

	sc_signal<sc_lv<NBIT> > inj1;
	sc_signal<sc_lv<NBIT> > inj2;
	sc_signal<sc_logic> inj3;
	sc_signal<sc_lv<NBIT> > obs1;

	MUX32b multiplexer32b;
	int check_result;

	SC_CTOR(TestBench) : multiplexer32b("multiplexer32b"){

		SC_THREAD(stimulus_thread);
		SC_METHOD(check_res);
			sensitive << obs1;
			dont_initialize();
		multiplexer32b.mux32b_A(inj1);
		multiplexer32b.mux32b_B(inj2);
		multiplexer32b.mux32b_sel(inj3);
		multiplexer32b.mux32b_OUT(obs1);
		init_values();
	}

	void check_res(){
		sc_lv<NBIT> a =obs1.read();
		std::cout << "Result from check_thread: " << a << std::endl;
		std::cout << "NumTestRes " << numTestRes << endl;
		std::cout << "Now at " << sc_time_stamp() << endl;
		std::cout << std::endl;
		if ( a != resVec[numTestRes])
			check_result = 1;
		numTestRes++;

	}

	private:

		void stimulus_thread(){

			std::cout << "__STIMULUS THREAD__" << std::endl;
			for (numTest=0; numTest<TEST_SIZE;numTest++){
	
				sc_lv<NBIT> a = values1[numTest];
				sc_lv<NBIT> b = values2[numTest];
				sc_logic c = values3[numTest];

				std::cout << "Stimulus_thread #"<<  numTest <<std::endl;
				std::cout << "Input 1 : "<< a << std::endl;
				std::cout << "Input 2 : "<< b << std::endl;
				std::cout << "Input 3 : "<< c << std::endl;

				inj1.write(a);
				inj2.write(b);
				inj3.write(c);

				wait(MS,SC_MS);
				}
			}

	unsigned numTest;
	unsigned numTestRes;
	static const unsigned MS = 10;
	static const unsigned TEST_SIZE = 3;

	sc_lv<NBIT> values1[TEST_SIZE],values2[TEST_SIZE];
	sc_logic values3[TEST_SIZE];

	sc_lv<NBIT> resVec[TEST_SIZE];

	void init_values(){

		std::cout << "Inizializzo Valori"<<std::endl;
		std::cout << std::endl;

		numTestRes = 0;
		check_result = 0;

		values1[0] = 15;
		values2[0] = 20;
		values3[0] = 1;
		resVec[0] = 20;

		values1[1] = 128;
		values2[1] = 196;
		values3[1] = 0;
		resVec[1] = 128;

		values1[2] = 17;
		values2[2] = 0;
		values3[2] = 1;
		resVec[2] = 0;
	}


};

int sc_main(int argc, char** argv){

	TestBench test("test");
	
	sc_start();

	return test.check_result;

}
	
