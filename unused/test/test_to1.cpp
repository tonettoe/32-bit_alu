#include <systemc.h>
#include <string>
#include "to1.hpp"

using namespace std;

SC_MODULE(TestBench){

	sc_signal<sc_lv<NBIT> > inj1;
	sc_signal<sc_logic> obs1;	//op1

	to1 to1comp;

	// Flags separati per la verifica del carry e del risultato
	int check_result;		

	SC_CTOR(TestBench)  :  to1comp("to1comp")
	{
		SC_THREAD(stimulus_thread);
		SC_METHOD(check_res);
			sensitive << obs1;
			dont_initialize();
		to1comp.to1_in(inj1);
		to1comp.to1_out(obs1);
		init_values();
	}

	void check_res(){
		sc_logic a =obs1.read();
		std::cout << "Result from check_thread: " << a << std::endl;
		std::cout << "NumTestRes " << numTestRes << endl;
		std::cout << "Now at " << sc_time_stamp() << endl;
		std::cout << std::endl;
		if ( a != resVec[numTestRes])
			check_result = 1;
		numTestRes++;
	}

	private:	
		
		void stimulus_thread(){

			std::cout << "__STIMULUS THREAD__" << std::endl;
			for (numTest=0; numTest<TEST_SIZE;numTest++){
	
				sc_lv<NBIT> a = values[numTest];

				std::cout << "Stimulus_thread #"<<  numTest <<std::endl;
				std::cout << "Input 1 : "<< a << std::endl;
				std::cout << std::endl;
				inj1.write(a);
				wait(MS,SC_MS);
				}
			}
	
	unsigned numTest;
	unsigned numTestRes;
	unsigned numTestCar;
	static const unsigned MS = 20;	
	static const unsigned TEST_SIZE = 2;

	//Vettori di ingresso

	sc_lv<NBIT> values[TEST_SIZE];

	//Vettori di uscita
	sc_logic resVec[TEST_SIZE];


	void init_values(){
		std::cout << "Inizializzo Valori"<<std::endl;
		std::cout << std::endl;
		
		numTestRes = 0;
		check_result= 0;


		values[0] = 0;
		resVec[0]= 0;

		values[1] = "11111111111111111111111111111111";
		resVec[1] = 1;
		


	}
};

int sc_main(int argc, char** argv){

	TestBench test("test");
	
	sc_start();

	return (test.check_result );

}
