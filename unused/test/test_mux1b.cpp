#include <systemc.h>
#include <string>
#include "mux1b.hpp"

using namespace std;

SC_MODULE(TestBench){

	sc_signal<sc_logic> inj0;
	sc_signal<sc_logic> inj1;
	sc_signal<sc_logic> inj2;
	sc_signal<sc_logic> obs;

	MUX1b multiplexer1b;
	int check_result;

	SC_CTOR(TestBench) : multiplexer1b("multiplexer1b")
	{
		SC_THREAD(stimulus_thread);
		SC_METHOD(check);
			sensitive << obs;
			dont_initialize();
		multiplexer1b.mux1b_sel(inj0);
		multiplexer1b.mux1b_a(inj1);
		multiplexer1b.mux1b_b(inj2);
		multiplexer1b.mux1b_out(obs);

		init_values();
	}


	void check(){
		sc_logic a =obs.read();
		std::cout << "Output from stimulus_thread: " << a << std::endl;
		if (values0[numTestRes] == 0)
			if ( a != (values1[numTestRes]))
				check_result = 1;
		if( a != (values2[numTestRes]))
			check_result=1;
		numTestRes++;
	}

	private:
	
		void stimulus_thread(){
			check_result =0;
			for (unsigned i=0; i<TEST_SIZE;i++){
				std::cout << "Stimulus_thread #"<<  i <<std::endl;
				std::cout << "Selettore " << values0[i] << " ingresso" <<std::endl;
				std::cout << "Input 1 : "<< values1[i] << std::endl;
				std::cout << "Input 2 : "<< values2[i] << std::endl;
				inj0.write(values0[i]);
				inj1.write(values1[i]);
				inj2.write(values2[i]);
				wait(MS,SC_MS);
			}
		}
	unsigned numTest;
	unsigned numTestRes;
	static const unsigned MS = 10;
	static const unsigned TEST_SIZE=2;
	sc_logic values0[TEST_SIZE],values1[TEST_SIZE],values2[TEST_SIZE];

	
	void init_values(){
		numTestRes =0;
		check_result=0;		
		values0[0] =0;
		values1[0] =0;
		values2[0] =0;

		values0[1] =1; 
		values1[1] =0;
		values2[1] =1;
	}
};

int sc_main(int argc, char** argv){

	TestBench test("test");
	
	sc_start();
	return test.check_result;	
	
}
