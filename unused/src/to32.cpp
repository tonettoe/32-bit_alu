#include <systemc.h>
#include "to32.hpp"

void to32::to32_thread() {

	while(true) {
		wait();
		sc_logic op =to32_in->read();
		sc_lv<NBIT> temp;
		for(int i=0;i<NBIT;i++){
			temp[i] = op;
		}
		wait(SC_ZERO_TIME);
		to32_out->write(temp);

	}
}
	
