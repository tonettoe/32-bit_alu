#include <systemc.h>
#include "to1.hpp"

void to1::to1_thread() {

	while(true) {
		wait();
		sc_lv<NBIT> op =to1_in->read();
		sc_logic temp =op[0];
		wait(SC_ZERO_TIME);
		to1_out->write(temp);

	}
}
	
