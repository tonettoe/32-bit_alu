#include <systemc.h>
#include "mux32b.hpp"

void MUX32b::mux32b_thread(){

	while(true){
		wait();

		mu_sel = mux32b_sel;

		sc_lv<NBIT> mux32b_A_in = mux32b_A->read();

		ma0 = mux32b_A_in[0];
		ma1 = mux32b_A_in[1];
		ma2 = mux32b_A_in[2];
		ma3 = mux32b_A_in[3];
		ma4 = mux32b_A_in[4];
		ma5 = mux32b_A_in[5];
		ma6 = mux32b_A_in[6];
		ma7 = mux32b_A_in[7];
		ma8 = mux32b_A_in[8];
		ma9 = mux32b_A_in[9];
		ma10 = mux32b_A_in[10];
		ma11 = mux32b_A_in[11];
		ma12 = mux32b_A_in[12];
		ma13 = mux32b_A_in[13];
		ma14 = mux32b_A_in[14];
		ma15 = mux32b_A_in[15];
		ma16 = mux32b_A_in[16];
		ma17 = mux32b_A_in[17];
		ma18 = mux32b_A_in[18];
		ma19 = mux32b_A_in[19];
		ma20 = mux32b_A_in[20];
		ma21 = mux32b_A_in[21];
		ma22 = mux32b_A_in[22];
		ma23 = mux32b_A_in[23];
		ma24 = mux32b_A_in[24];
		ma25 = mux32b_A_in[25];
		ma26 = mux32b_A_in[26];
		ma27 = mux32b_A_in[27];
		ma28 = mux32b_A_in[28];
		ma29 = mux32b_A_in[29];
		ma30 = mux32b_A_in[30];
		ma31 = mux32b_A_in[31];

		sc_lv<NBIT> mux32b_B_in = mux32b_B->read();

		mb0 = mux32b_B_in[0];
		mb1 = mux32b_B_in[1];
		mb2 = mux32b_B_in[2];
		mb3 = mux32b_B_in[3];
		mb4 = mux32b_B_in[4];
		mb5 = mux32b_B_in[5];
		mb6 = mux32b_B_in[6];
		mb7 = mux32b_B_in[7];
		mb8 = mux32b_B_in[8];
		mb9 = mux32b_B_in[9];
		mb10 = mux32b_B_in[10];
		mb11 = mux32b_B_in[11];
		mb12 = mux32b_B_in[12];
		mb13 = mux32b_B_in[13];
		mb14 = mux32b_B_in[14];
		mb15 = mux32b_B_in[15];
		mb16 = mux32b_B_in[16];
		mb17 = mux32b_B_in[17];
		mb18 = mux32b_B_in[18];
		mb19 = mux32b_B_in[19];
		mb20 = mux32b_B_in[20];
		mb21 = mux32b_B_in[21];
		mb22 = mux32b_B_in[22];
		mb23 = mux32b_B_in[23];
		mb24 = mux32b_B_in[24];
		mb25 = mux32b_B_in[25];
		mb26 = mux32b_B_in[26];
		mb27 = mux32b_B_in[27];
		mb28 = mux32b_B_in[28];
		mb29 = mux32b_B_in[29];
		mb30 = mux32b_B_in[30];
		mb31 = mux32b_B_in[31];

		wait(SC_ZERO_TIME);		
		
		to_OUT[0] = mo0;
		to_OUT[1] = mo1;
		to_OUT[2] = mo2;
		to_OUT[3] = mo3;
		to_OUT[4] = mo4;
		to_OUT[5] = mo5;
		to_OUT[6] = mo6;
		to_OUT[7] = mo7;
		to_OUT[8] = mo8;
		to_OUT[9] = mo9;
		to_OUT[10] = mo10;
		to_OUT[11] = mo11;
		to_OUT[12] = mo12;
		to_OUT[13] = mo13;
		to_OUT[14] = mo14;
		to_OUT[15] = mo15;
		to_OUT[16] = mo16;
		to_OUT[17] = mo17;
		to_OUT[18] = mo18;
		to_OUT[19] = mo19;
		to_OUT[20] = mo20;
		to_OUT[21] = mo21;
		to_OUT[22] = mo22;
		to_OUT[23] = mo23;
		to_OUT[24] = mo24;
		to_OUT[25] = mo25;
		to_OUT[26] = mo26;
		to_OUT[27] = mo27;
		to_OUT[28] = mo28;
		to_OUT[29] = mo29;
		to_OUT[30] = mo30;
		to_OUT[31] = mo31;
		
		mux32b_OUT->write(to_OUT);
	}
}

