#include <systemc.h>
#include "buffer1b.hpp"

void BUFFER1b::buffer1b_thread(){

	while(true) {
		wait();
		input =buffer1b_in->read();
		wait(SC_ZERO_TIME);
		wait(2,SC_NS);
		buffer1b_out->write(input);
	}
}
