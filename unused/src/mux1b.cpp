#include <systemc.h>
#include "mux1b.hpp"

void MUX1b::mux1b_thread() {

	while(true) {
		wait();
		sc_logic s= mux1b_sel->read();
		sc_logic ain = mux1b_a->read();
		sc_logic bin = mux1b_b->read();
		wait(SC_ZERO_TIME);
		if( s == 0 )
		 mux1b_out->write( ain);
		else
		mux1b_out->write(bin);
	}
}
