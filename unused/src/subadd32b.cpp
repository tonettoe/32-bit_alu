#include <systemc.h>
#include "subadd32b.hpp"

void SUBADD32b::subadd32b_thread_input(){

	while(true){
		wait();

		A = subadd32b_A_in->read();
		B = subadd32b_B_in->read();
		OP = subadd32b_OP_in->read();

	}
}

void SUBADD32b::subadd32b_thread_output(){

	while(true){
		wait();
		std::cout << "_____________________A VALE: " << A << std::endl;
		std::cout << "_____________________B VALE: " << B << std::endl;
		std::cout << "_____________________TO_FLA VALE: " << to_FLA << std::endl;
		std::cout << "_____________________TO_FLB VALE: " << to_FLB << std::endl;
		std::cout << "_____________________OP VALE:    " << OP << std::endl;
		std::cout << "_____________________TO_FLOP VALE: " << to_FLOP << std::endl;

		std::cout << "Thread subadd32bit Now at " << sc_time_stamp() << endl;
		std::cout << endl;
		subadd32b_R_out->write(to_OUT32);
		subadd32b_carry_out->write(to_OUTcarry);


	}
}

