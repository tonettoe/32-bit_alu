#ifndef MUX1B_HPP
#define MUX1B_HPP

SC_MODULE(MUX1b) { 

	sc_in <sc_logic> mux1b_sel;
	sc_in <sc_logic> mux1b_a;
	sc_in <sc_logic> mux1b_b;
	sc_out<sc_logic> mux1b_out;

	SC_CTOR(MUX1b){
		SC_THREAD(mux1b_thread);
		sensitive << mux1b_a 
			  << mux1b_b
			  << mux1b_sel;
	}
	
	void mux1b_thread();
};

#endif
