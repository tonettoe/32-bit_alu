#ifndef TO1B_HPP
#define TO1B_HPP

#include <systemc.h>
#include <string>

#define NBIT 32

SC_MODULE(to1) {

	sc_in<sc_lv<NBIT> > to1_in;
	sc_out<sc_logic> to1_out;

	
	SC_CTOR(to1)	{
		SC_THREAD(to1_thread);
			sensitive << to1_in;
				;
			}
	
		void to1_thread(void);
};
#endif

