#ifndef BUFFER1B_HPP
#define BUFFER1B_HPP

SC_MODULE(BUFFER1b) {

	sc_in<sc_logic> buffer1b_in;
	sc_out<sc_logic> buffer1b_out;
	sc_signal<sc_logic> input;

	SC_CTOR(BUFFER1b){
		SC_THREAD(buffer1b_thread);
		sensitive << buffer1b_in;
	}
		
	void buffer1b_thread();
};

#endif
