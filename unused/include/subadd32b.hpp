#ifndef SUBADD32B_HPP
#define SUBADD32B_HPP

#include <systemc.h>
#include <string>
#include "xor32b.hpp"
#include "fa32b.hpp"
#include "to32.hpp"
#include "buffer32b.hpp"
#include "to1.hpp"


#define NBIT 32

SC_MODULE(SUBADD32b) {

	sc_in<sc_lv<NBIT> > subadd32b_A_in, subadd32b_B_in;
	sc_in< sc_logic> subadd32b_OP_in;

	sc_out<sc_lv<NBIT> > subadd32b_R_out;
	sc_out<sc_logic > subadd32b_carry_out;


	sc_signal<sc_lv<NBIT> > A,B, to_OUT32, OP32,to_FLA,to_FLB,OP32delay,Bdelay,Adelay;
	sc_signal<sc_logic> OP,to_OUTcarry,to_FLOP, OP1;



	//inner components
	FA32b f32_0;
	XOR32b x32_0;
	to32 to32component;
	BUFFER32b buff32b_1,buff32b_3,buff32b_4;
	BUFFER1b buff1b_1,buff1b_2;

	SC_CTOR(SUBADD32b) : f32_0("f32_0"),x32_0("x32_0"),to32component("to32component"), buff32b_1("buff32b_1"),buff1b_1("buff1b_1"),buff1b_2("buff1b_2"),buff32b_3("buff32b_3"),buff32b_4("buff32b_4"){
		//Convert 1bit OP input in 32bit	
		to32component.to32_in(OP);
		to32component.to32_out(OP32);		
		
		buff1b_1.buffer1b_in(OP);
		buff1b_1.buffer1b_out(OP1);

		buff1b_2.buffer1b_in(OP1);
		buff1b_2.buffer1b_out(to_FLOP);


		// Delay for input A
		buff32b_4.buffer32b_in(A);
		buff32b_4.buffer32b_out(Adelay);


		buff32b_1.buffer32b_in(Adelay);
		buff32b_1.buffer32b_out(to_FLA);



		//Delay for input B
		buff32b_3.buffer32b_in(B);
		buff32b_3.buffer32b_out(Bdelay);
		
		x32_0.xor32b_A(Bdelay);
		x32_0.xor32b_B(OP32);
		x32_0.xor32b_R(to_FLB);

		f32_0.fa32b_A_in(to_FLA);
		f32_0.fa32b_B_in(to_FLB);
		f32_0.fa32b_c0_in(to_FLOP);
		f32_0.fa32b_R_out(to_OUT32);
		f32_0.fa32b_c32_out(to_OUTcarry);				

		SC_THREAD(subadd32b_thread_input);
			sensitive << subadd32b_A_in << subadd32b_B_in << subadd32b_OP_in;
		SC_THREAD(subadd32b_thread_output);
			sensitive << to_FLB << to_FLA;
		;
	}
	
	void subadd32b_thread_input(void);
	void subadd32b_thread_output(void);

};
#endif

