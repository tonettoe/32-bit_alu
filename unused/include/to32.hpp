#ifndef TO32B_HPP
#define TO32B_HPP

#include <systemc.h>
#include <string>

#define NBIT 32

SC_MODULE(to32) {

	sc_in<sc_logic> to32_in;
	sc_out<sc_lv<NBIT> > to32_out;
	
	SC_CTOR(to32)	{
		SC_THREAD(to32_thread);
			sensitive << to32_in;
				;
			}
	
		void to32_thread(void);
};
#endif

