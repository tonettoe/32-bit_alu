#ifndef MUX32B_HPP
#define MUX32B_HPP

#include <systemc.h>
#include <string>
#include "mux1b.hpp"

#define NBIT 32

SC_MODULE(MUX32b) {

	sc_in<sc_logic > mux32b_sel;
	sc_in<sc_lv<NBIT> > mux32b_A, mux32b_B;
	sc_out<sc_lv<NBIT> > mux32b_OUT;

	//inner signal


	sc_signal<sc_logic> ma0,ma1,ma2,ma3,ma4,ma5,ma6,ma7,ma8,ma9,ma10,ma11,ma12,ma13,ma14,ma15,ma16,ma17,ma18,ma19,ma20,ma21,ma22,ma23,ma24,ma25,ma26,ma27,ma28,ma29,ma30,ma31;
	sc_signal<sc_logic> mb0,mb1,mb2,mb3,mb4,mb5,mb6,mb7,mb8,mb9,mb10,mb11,mb12,mb13,mb14,mb15,mb16,mb17,mb18,mb19,mb20,mb21,mb22,mb23,mb24,mb25,mb26,mb27,mb28,mb29,mb30,mb31;
	sc_signal<sc_logic> mo0,mo1,mo2,mo3,mo4,mo5,mo6,mo7,mo8,mo9,mo10,mo11,mo12,mo13,mo14,mo15,mo16,mo17,mo18,mo19,mo20,mo21,mo22,mo23,mo24,mo25,mo26,mo27,mo28,mo29,mo30,mo31;
	sc_signal<sc_logic> mu_sel;
				
	sc_lv<NBIT> to_OUT; //Logic Vector per accumulare i valori, singoli bit provenienti dai singoli mux1b a logic vector 				

	//inner components
	MUX1b m0,m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13,m14,m15,m16,m17,m18,m19,m20,m21,m22,m23,m24,m25,m26,m27,m28,m29,m30,m31;

	SC_CTOR(MUX32b): m0("m0"),m1("m1"),m2("m2"),m3("m3"),m4("m4"),m5("m5"),m6("m6"),m7("m7"),m8("m8"),m9("m9"),m10("m10"),m11("m11"),m12("m12"),m13("m13"),m14("m14"),m15("m15"),m16("m16"),
			m17("m17"),m18("m18"),m19("m19"),m20("m20"),m21("m21"),m22("m22"),m23("m23"),m24("m24"),m25("m25"),m26("m26"),m27("m27"),m28("m28"),m29("m29"),m30("m30"),m31("m31"){


		m0.mux1b_sel(mu_sel);
		m0.mux1b_a(ma0);
		m0.mux1b_b(mb0);
		m0.mux1b_out(mo0);
	
		m1.mux1b_sel(mu_sel);
		m1.mux1b_a(ma1);
		m1.mux1b_b(mb1);
		m1.mux1b_out(mo1);

		m2.mux1b_sel(mu_sel);
		m2.mux1b_a(ma2);
		m2.mux1b_b(mb2);
		m2.mux1b_out(mo2);

		m3.mux1b_sel(mu_sel);
		m3.mux1b_a(ma3);
		m3.mux1b_b(mb3);
		m3.mux1b_out(mo3);

		m4.mux1b_sel(mu_sel);
		m4.mux1b_a(ma4);
		m4.mux1b_b(mb4);
		m4.mux1b_out(mo4);

		m5.mux1b_sel(mu_sel);
		m5.mux1b_a(ma5);
		m5.mux1b_b(mb5);
		m5.mux1b_out(mo5);

		m6.mux1b_sel(mu_sel);
		m6.mux1b_a(ma6);
		m6.mux1b_b(mb6);
		m6.mux1b_out(mo6);

		m7.mux1b_sel(mu_sel);
		m7.mux1b_a(ma7);
		m7.mux1b_b(mb7);
		m7.mux1b_out(mo7);

		m8.mux1b_sel(mu_sel);
		m8.mux1b_a(ma8);
		m8.mux1b_b(mb8);
		m8.mux1b_out(mo8);

		m9.mux1b_sel(mu_sel);
		m9.mux1b_a(ma9);
		m9.mux1b_b(mb9);
		m9.mux1b_out(mo9);

		m10.mux1b_sel(mu_sel);
		m10.mux1b_a(ma10);
		m10.mux1b_b(mb10);
		m10.mux1b_out(mo10);

		m11.mux1b_sel(mu_sel);
		m11.mux1b_a(ma11);
		m11.mux1b_b(mb11);
		m11.mux1b_out(mo11);

		m12.mux1b_sel(mu_sel);
		m12.mux1b_a(ma12);
		m12.mux1b_b(mb12);
		m12.mux1b_out(mo12);

		m13.mux1b_sel(mu_sel);
		m13.mux1b_a(ma13);
		m13.mux1b_b(mb13);
		m13.mux1b_out(mo13);

		m14.mux1b_sel(mu_sel);
		m14.mux1b_a(ma14);
		m14.mux1b_b(mb14);
		m14.mux1b_out(mo14);

		m15.mux1b_sel(mu_sel);
		m15.mux1b_a(ma15);
		m15.mux1b_b(mb15);
		m15.mux1b_out(mo15);

		m16.mux1b_sel(mu_sel);
		m16.mux1b_a(ma16);
		m16.mux1b_b(mb16);
		m16.mux1b_out(mo16);

		m17.mux1b_sel(mu_sel);
		m17.mux1b_a(ma17);
		m17.mux1b_b(mb17);
		m17.mux1b_out(mo17);

		m18.mux1b_sel(mu_sel);
		m18.mux1b_a(ma18);
		m18.mux1b_b(mb18);
		m18.mux1b_out(mo18);

		m19.mux1b_sel(mu_sel);
		m19.mux1b_a(ma19);
		m19.mux1b_b(mb19);
		m19.mux1b_out(mo19);

		m20.mux1b_sel(mu_sel);
		m20.mux1b_a(ma20);
		m20.mux1b_b(mb20);
		m20.mux1b_out(mo20);

		m21.mux1b_sel(mu_sel);
		m21.mux1b_a(ma21);
		m21.mux1b_b(mb21);
		m21.mux1b_out(mo21);

		m22.mux1b_sel(mu_sel);
		m22.mux1b_a(ma22);
		m22.mux1b_b(mb22);
		m22.mux1b_out(mo22);

		m23.mux1b_sel(mu_sel);
		m23.mux1b_a(ma23);
		m23.mux1b_b(mb23);
		m23.mux1b_out(mo23);

		m24.mux1b_sel(mu_sel);
		m24.mux1b_a(ma24);
		m24.mux1b_b(mb24);
		m24.mux1b_out(mo24);

		m25.mux1b_sel(mu_sel);
		m25.mux1b_a(ma25);
		m25.mux1b_b(mb25);
		m25.mux1b_out(mo25);

		m26.mux1b_sel(mu_sel);
		m26.mux1b_a(ma26);
		m26.mux1b_b(mb26);
		m26.mux1b_out(mo26);

		m27.mux1b_sel(mu_sel);
		m27.mux1b_a(ma27);
		m27.mux1b_b(mb27);
		m27.mux1b_out(mo27);

		m28.mux1b_sel(mu_sel);
		m28.mux1b_a(ma28);
		m28.mux1b_b(mb28);
		m28.mux1b_out(mo28);

		m29.mux1b_sel(mu_sel);
		m29.mux1b_a(ma29);
		m29.mux1b_b(mb29);
		m29.mux1b_out(mo29);

		m30.mux1b_sel(mu_sel);
		m30.mux1b_a(ma30);
		m30.mux1b_b(mb30);
		m30.mux1b_out(mo30);

		m31.mux1b_sel(mu_sel);
		m31.mux1b_a(ma31);
		m31.mux1b_b(mb31);
		m31.mux1b_out(mo31);



		SC_THREAD(mux32b_thread);
			sensitive << mux32b_A << mux32b_B << mux32b_sel;
		;
	}

	void mux32b_thread(void);
};
#endif
		


				
		
