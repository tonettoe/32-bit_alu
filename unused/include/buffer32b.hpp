#ifndef BUFFER32B_HPP
#define BUFFER32B_HPP

#include <systemc.h>
#include <string>
#include "buffer1b.hpp"
#include "conv32to1.hpp"
#include "conv1to32.hpp"

#define NBIT 32

SC_MODULE(BUFFER32b) {

	sc_in<sc_lv<NBIT> > buffer32b_in;
	sc_out<sc_lv<NBIT> > buffer32b_out;
	
	sc_signal<sc_logic> aa0,aa1,aa2,aa3,aa4,aa5,aa6,aa7,aa8,aa9,aa10,aa11,aa12,aa13,aa14,aa15,aa16,aa17,aa18,aa19,aa20,aa21,aa22,aa23,aa24,aa25,aa26,aa27,aa28,aa29,aa30,aa31;
	sc_signal<sc_logic> ao0,ao1,ao2,ao3,ao4,ao5,ao6,ao7,ao8,ao9,ao10,ao11,ao12,ao13,ao14,ao15,ao16,ao17,ao18,ao19,ao20,ao21,ao22,ao23,ao24,ao25,ao26,ao27,ao28,ao29,ao30,ao31;	
	sc_signal<sc_lv<NBIT> > conv1in_frominput, buffer32b_forout;

	//inner components
	BUFFER1b a0,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18,a19,a20,a21,a22,a23,a24,a25,a26,a27,a28,a29,a30,a31;
	conv1to32 conv1in;
	conv32to1 convout;
	SC_CTOR(BUFFER32b) : a0("a0"),a1("a1"),a2("a2"),a3("a3"),a4("a4"),a5("a5"),a6("a6"),a7("a7"),a8("a8"),a9("a9"),a10("a10"),a11("a11"),a12("a12"),a13("a13"),a14("a14"),a15("a15"),a16("a16"),a17("a17"),a18("a18"),a19("a19"),a20("a20"),a21("a21"),a22("a22"),a23("a23"),a24("a24"),a25("a25"),a26("a26"),a27("a27"),a28("a28"),a29("a29"),a30("a30"),a31("a31"),conv1in("conv1in"),convout("convout")
				{

				a0.buffer1b_in(aa0);
				a0.buffer1b_out(ao0);
				
				a1.buffer1b_in(aa1);
				a1.buffer1b_out(ao1);

				a2.buffer1b_in(aa2);
				a2.buffer1b_out(ao2);
				
				a3.buffer1b_in(aa3);
				a3.buffer1b_out(ao3);
				
				a4.buffer1b_in(aa4);
				a4.buffer1b_out(ao4);
				
				a5.buffer1b_in(aa5);
				a5.buffer1b_out(ao5);
				
				a6.buffer1b_in(aa6);
				a6.buffer1b_out(ao6);
				
				a7.buffer1b_in(aa7);
				a7.buffer1b_out(ao7);
				
				a8.buffer1b_in(aa8);
				a8.buffer1b_out(ao8);
				
				a9.buffer1b_in(aa9);
				a9.buffer1b_out(ao9);
				
				a10.buffer1b_in(aa10);
				a10.buffer1b_out(ao10);
				
				a11.buffer1b_in(aa11);
				a11.buffer1b_out(ao11);
				
				a12.buffer1b_in(aa12);
				a12.buffer1b_out(ao12);
				
				a13.buffer1b_in(aa13);
				a13.buffer1b_out(ao13);
				
				a14.buffer1b_in(aa14);
				a14.buffer1b_out(ao14);
				
				a15.buffer1b_in(aa15);
				a15.buffer1b_out(ao15);
				
				a16.buffer1b_in(aa16);
				a16.buffer1b_out(ao16);
				
				a17.buffer1b_in(aa17);
				a17.buffer1b_out(ao17);
				
				a18.buffer1b_in(aa18);
				a18.buffer1b_out(ao18);
				
				a19.buffer1b_in(aa19);
				a19.buffer1b_out(ao19);
				// a20
				a20.buffer1b_in(aa20);
				a20.buffer1b_out(ao20);
				
				a21.buffer1b_in(aa21);
				a21.buffer1b_out(ao21);
				
				a22.buffer1b_in(aa22);
				a22.buffer1b_out(ao22);
				
				a23.buffer1b_in(aa23);
				a23.buffer1b_out(ao23);
				
				a24.buffer1b_in(aa24);
				a24.buffer1b_out(ao24);
				
				a25.buffer1b_in(aa25);
				a25.buffer1b_out(ao25);
				
				a26.buffer1b_in(aa26);
				a26.buffer1b_out(ao26);
				
				a27.buffer1b_in(aa27);
				a27.buffer1b_out(ao27);
				
				a28.buffer1b_in(aa28);
				a28.buffer1b_out(ao28);
				
				a29.buffer1b_in(aa29);
				a29.buffer1b_out(ao29);
				
				a30.buffer1b_in(aa30);
				a30.buffer1b_out(ao30);
				
				a31.buffer1b_in(aa31);
				a31.buffer1b_out(ao31);

				conv1in.conv_in(conv1in_frominput);	
				conv1in.conv0_out(aa0);
				conv1in.conv1_out(aa1);
				conv1in.conv2_out(aa2);
				conv1in.conv3_out(aa3);
				conv1in.conv4_out(aa4);
				conv1in.conv5_out(aa5);
				conv1in.conv6_out(aa6);
				conv1in.conv7_out(aa7);
				conv1in.conv8_out(aa8);
				conv1in.conv9_out(aa9);
				conv1in.conv10_out(aa10);
				conv1in.conv11_out(aa11);
				conv1in.conv12_out(aa12);
				conv1in.conv13_out(aa13);
				conv1in.conv14_out(aa14);
				conv1in.conv15_out(aa15);
				conv1in.conv16_out(aa16);
				conv1in.conv17_out(aa17);
				conv1in.conv18_out(aa18);
				conv1in.conv19_out(aa19);
				conv1in.conv20_out(aa20);
				conv1in.conv21_out(aa21);
				conv1in.conv22_out(aa22);
				conv1in.conv23_out(aa23);
				conv1in.conv24_out(aa24);
				conv1in.conv25_out(aa25);
				conv1in.conv26_out(aa26);
				conv1in.conv27_out(aa27);
				conv1in.conv28_out(aa28);
				conv1in.conv29_out(aa29);
				conv1in.conv30_out(aa30);
				conv1in.conv31_out(aa31);

				convout.conv_out(buffer32b_forout);
				convout.conv0_in(ao0);
				convout.conv1_in(ao1);
				convout.conv2_in(ao2);
				convout.conv3_in(ao3);
				convout.conv4_in(ao4);
				convout.conv5_in(ao5);
				convout.conv6_in(ao6);
				convout.conv7_in(ao7);
				convout.conv8_in(ao8);
				convout.conv9_in(ao9);
				convout.conv10_in(ao10);
				convout.conv11_in(ao11);
				convout.conv12_in(ao12);
				convout.conv13_in(ao13);
				convout.conv14_in(ao14);
				convout.conv15_in(ao15);
				convout.conv16_in(ao16);
				convout.conv17_in(ao17);
				convout.conv18_in(ao18);
				convout.conv19_in(ao19);
				convout.conv20_in(ao20);
				convout.conv21_in(ao21);
				convout.conv22_in(ao22);
				convout.conv23_in(ao23);
				convout.conv24_in(ao24);
				convout.conv25_in(ao25);
				convout.conv26_in(ao26);
				convout.conv27_in(ao27);
				convout.conv28_in(ao28);
				convout.conv29_in(ao29);
				convout.conv30_in(ao30);
				convout.conv31_in(ao31);
		
				SC_THREAD(buffer32b_thread);
					sensitive << buffer32b_in;
				;
			}
	
		void buffer32b_thread(void);
};
#endif

