#include <systemc.h>
#include "shifterleft.hpp"

void SHIFTERLEFT::shifterleft_thread() {

	while(true) {
		wait();
		sc_lv<NBIT> shl = shifterleft_A->read();
		shl = shl << 1;
		wait(SC_ZERO_TIME);
		shifterleft_R->write(shl);
	}
}
	
