#include <systemc.h>
#include "conv1to32.hpp"

void conv1to32::conv1to32_thread() {

	while(true) {
		wait();
		A_in=conv_in->read();

		c0 = A_in[0];
		c1 = A_in[1];
		c2 = A_in[2];
		c3 = A_in[3];
		c4 = A_in[4];
		c5 = A_in[5];
		c6 = A_in[6];
		c7 = A_in[7];
		c8 = A_in[8];
		c9 = A_in[9];
		c10 = A_in[10];
		c11 = A_in[11];
		c12 = A_in[12];
		c13 = A_in[13];
		c14 = A_in[14];
		c15 = A_in[15];
		c16 = A_in[16];
		c17 = A_in[17];
		c18 = A_in[18];
		c19 = A_in[19];
		c20 = A_in[20];
		c21 = A_in[21];
		c22 = A_in[22];
		c23 = A_in[23];
		c24 = A_in[24];
		c25 = A_in[25];
		c26 = A_in[26];
		c27 = A_in[27];
		c28 = A_in[28];
		c29 = A_in[29];
		c30 = A_in[30];
		c31 = A_in[31];
		
		wait(SC_ZERO_TIME);

		
 		conv0_out->write(c0);
		conv1_out->write(c1);
		conv2_out->write(c2);
		conv3_out->write(c3);
		conv4_out->write(c4);
		conv5_out->write(c5);
		conv6_out->write(c6);
		conv7_out->write(c7);
		conv8_out->write(c8);
		conv9_out->write(c9);
		conv10_out->write(c10);
		conv11_out->write(c11);
		conv12_out->write(c12);
		conv13_out->write(c13);
		conv14_out->write(c14);
		conv15_out->write(c15);
		conv16_out->write(c16);
		conv17_out->write(c17);
		conv18_out->write(c18);
		conv19_out->write(c19);
		conv20_out->write(c20);
		conv21_out->write(c21);
		conv22_out->write(c22);
		conv23_out->write(c23);
		conv24_out->write(c24);
		conv25_out->write(c25);
		conv26_out->write(c26);
		conv27_out->write(c27);
		conv28_out->write(c28);
		conv29_out->write(c29);
		conv30_out->write(c30);
		conv31_out->write(c31);		

	}
}
	
