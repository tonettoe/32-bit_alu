#include <systemc.h>
#include "not1b.hpp"

void NOT1b::not1b_thread(){

	while(true) {
		wait();
		sc_logic input =not_in->read();
		wait(SC_ZERO_TIME);
		not_out->write(~input);
	}
}
