#include <systemc.h>
#include "xor1b.hpp"

void XOR1b::xor1b_thread() {

	while(true) {
		wait();
		sc_logic input1=xor_in1->read();
		sc_logic input2=xor_in2->read();
		wait(SC_ZERO_TIME);
		xor_out->write( input1 ^ input2 );
	}
}
