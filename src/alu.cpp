#include <systemc.h>
#include <string>
#include "alu.hpp"

void ALU::alu_thread() {

		zero=0;
		CARRYin.write(zero);

	while(true) {

		wait();
		wait(SC_ZERO_TIME);
		Ain=A->read();
		Bin=B->read();

		OPin=OP->read();

		wait(SC_ZERO_TIME);

		RES->write(RESout);
		CARRY->write(CARRYout);
	}
}
	
