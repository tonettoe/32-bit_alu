#include <systemc.h>
#include "and1b.hpp"

void AND1b::and1b_thread() {

	while(true) {
		wait();
		sc_logic input1=and_in1->read();
		sc_logic input2=and_in2->read();
		wait(SC_ZERO_TIME);
		and_out->write( input1 & input2 );
	}
}
	
