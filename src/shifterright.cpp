#include <systemc.h>
#include "shifterright.hpp"

void SHIFTERRIGHT::shifterright_thread() {

	while(true) {
		wait();
		sc_lv<NBIT> shr = shifterright_A->read();
		shr = shr >> 1;
		wait(SC_ZERO_TIME);
		shifterright_R->write(shr);
	}
}
	
