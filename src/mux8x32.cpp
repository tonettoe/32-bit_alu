#include <systemc.h>
#include "mux8x32.hpp"

void MUX8x32::mux8x32_thread() {

	while(true) {

		wait();
		sc_uint<NSEL> s = mux8x32_sel->read();
		
		sc_lv<NBIT> in0 = mux8x32_0->read();
		sc_lv<NBIT> in1 = mux8x32_1->read();
		sc_lv<NBIT> in2 = mux8x32_2->read();
		sc_lv<NBIT> in3 = mux8x32_3->read();
		sc_lv<NBIT> in4 = mux8x32_4->read();
		sc_lv<NBIT> in5 = mux8x32_5->read();
		sc_lv<NBIT> in6 = mux8x32_6->read();
		sc_lv<NBIT> in7 = mux8x32_7->read();
		sc_lv<NBIT> to_out;
		switch(s){
			case 0: to_out=in0;
			break;
			case 1: to_out=in1;
			break;
			case 2: to_out=in2;
			break;
			case 3: to_out=in3;
			break;
			case 4: to_out=in4;
			break;
			case 5: to_out=in5;
			break;
			case 6: to_out=in6;
			break;
			case 7: to_out=in7;
			break;
		}
		wait(SC_ZERO_TIME);
		mux8x32_OUT->write(to_out);
	}
}
	
