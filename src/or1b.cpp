#include <systemc.h>
#include "or1b.hpp"

void OR1b::or1b_thread() {

	while(true) {
		wait();
		sc_logic input1=or_in1->read();
		sc_logic input2=or_in2->read();
		wait(SC_ZERO_TIME);
		or_out->write(input1|input2);
	}
}
	 
