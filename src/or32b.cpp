#include <systemc.h>
#include "or32b.hpp"

void OR32b::or32b_thread(){

	while(true){
		wait();
		conv1in_frominput = or32b_A_in->read();
		
		conv2in_frominput = or32b_B_in->read();

		wait(2,SC_NS);
		
		or32b_R_out->write(or32b_R_forout);
	}
}
