#include <systemc.h>
#include "conv32to1.hpp"

void conv32to1::conv32to1_thread() {

	while(true) {
		wait();	
		A_out[0]=conv0_in->read();
		A_out[1]=conv1_in->read();
		A_out[2]=conv2_in->read();
		A_out[3]=conv3_in->read();
		A_out[4]=conv4_in->read();
		A_out[5]=conv5_in->read();
		A_out[6]=conv6_in->read();
		A_out[7]=conv7_in->read();
		A_out[8]=conv8_in->read();
		A_out[9]=conv9_in->read();
		A_out[10]=conv10_in->read();
		A_out[11]=conv11_in->read();
		A_out[12]=conv12_in->read();
		A_out[13]=conv13_in->read();
		A_out[14]=conv14_in->read();
		A_out[15]=conv15_in->read();
		A_out[16]=conv16_in->read();
		A_out[17]=conv17_in->read();
		A_out[18]=conv18_in->read();
		A_out[19]=conv19_in->read();
		A_out[20]=conv20_in->read();
		A_out[21]=conv21_in->read();
		A_out[22]=conv22_in->read();
		A_out[23]=conv23_in->read();
		A_out[24]=conv24_in->read();
		A_out[25]=conv25_in->read();
		A_out[26]=conv26_in->read();
		A_out[27]=conv27_in->read();
		A_out[28]=conv28_in->read();
		A_out[29]=conv29_in->read();
		A_out[30]=conv30_in->read();
		A_out[31]=conv31_in->read();

		wait(SC_ZERO_TIME);

		conv_out-> write(A_out);

	}
}
	
