#include <systemc.h>
#include "and32b.hpp"

void AND32b::and32b_thread(){

	while(true){
		wait();

		conv1in_frominput = and32b_A->read();
		
		conv2in_frominput = and32b_B->read();

		wait(2,SC_NS);

		and32b_R->write(and32b_R_forout);

	}
}


