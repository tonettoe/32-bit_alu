#include <systemc.h>
#include "fa32b.hpp"
// Thread per assegnare i singoli ingressi ai singoli fa1b a partire dal logic vector presente in ingresso
void FA32b::fa32b_thread(){

	while(true) {
		wait();
		c0=fa32b_c0_in->read();

		conv1in_frominput = fa32b_A_in->read();

		conv2in_frominput = fa32b_B_in->read();

		wait(2,SC_NS);

	        fa32b_R_out->write(fa32b_R);		
	        fa32b_c32_out->write(c32);
	}
}

