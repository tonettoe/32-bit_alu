#include <systemc.h>
#include "xor32b.hpp"

void XOR32b::xor32b_thread(){

	while(true){
		wait();

		conv1in_frominput = xor32b_A->read();

		conv2in_frominput = xor32b_B->read();

		wait(2,SC_NS);
		
		xor32b_R->write(xor32b_R_forout);
	}
}
